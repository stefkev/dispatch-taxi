config = require "./../config"
mailer = require "./mailer.coffee"

providersList = config.SMS_PROVIDER_LIST

module.exports.send = (number, contents, cb)->
  for address in providersList
    mailer.sendMail(number + "@" + address, "" , contents, cb)