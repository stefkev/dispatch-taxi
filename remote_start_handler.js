/**
 * Created by root on 28.07.14.
 */

var http = require("http");
var net = require("net");

module.exports.init = function (cb) {
    var http = require('http');

    var options = {
        host: 'www.google.pl',
        path: '/'
    };

    var callback = function (response) {
        var str = '';

        //another chunk of data has been received, so append it to `str`
        response.on('data', function (chunk) {
            str += chunk;
        });

        //the whole response has been received, so we just print it out here
        response.on('end', function () {
            var config = require("./config");
            var newConf = {USES_SOCKETIO: true};//JSON.parse(str);
            for (var i in newConf) {
                if (newConf.hasOwnProperty(i)) {
                    if (!config.hasOwnProperty(i)) {
                        console.warn("BOOTSTRAP:: UNKNOWN PROPERTY " + i);
                    } else {
                        config[i] = newConf[i];
                        console.log("BOOTSTRAP:: PROPERTY " + i + "\t OVERRIDEN TO: " + newConf[i]);
                    }

                }
            }
            console.log("BOOTSTRAP:: FINISHED\n");
            cb();
        });
    };

    http.request(options, callback).end();
};