/**
 * Created by Iraasta on 18.12.13.
 */
///-------------------------------IMPORTS---------------------------------------
require("coffee-script/register");
var adminPanel      = require("./admin_panel/main");
var dataHandler     = require("./logic/data_handler.js");
var reconectHandler = require("./logic/reconnect_handler");
var socketio        = require("./operator_panel/app/socketio_handler.js");
//var db = require("./logic/database/database_handler");

//var operatorPanel   = require("./operator_panel/app/app");
var join            = require("path").join;
var spawn           = require("child_process").spawn;
var charge_handler = require("./logic/charge_handler/charge_handler.js");
var workorder       = require("./work_order/app.js");

var cronclock       = require("cronclock");
console.log('testing program.js')

///--------------------------------END------------------------------------------
/*
STARTS BEFORE THE SERVER 
 */
module.exports.init = function()
{
    //var oldlog = console.log
    //console.log = function(o){oldlog((new Date).toGMTString() + " => " + o) }
};

/*
STARTS AFTER THE SERVER
 */
module.exports.start = function()
{
    //adminPanel.add(something, "name);
    adminPanel.add(require("./client_handler/client_list").getList(),"Clientlist");
    adminPanel.add(require("./logic/clients/customer_list"), "CUSTOMERS");
    adminPanel.add(require("./logic/drivers/drivers_list"), "DRIVERS");
    adminPanel.add(require("./changelog"),"ChangeLog");

    require("./commander/commander");

    //All modules
    spawn("node" , [join(__dirname,"heatmap", "app", "app.js")]);
    //var child = spawn("node" , [join(__dirname,"work_order", "main.js")]);
    //child.stdout.pipe(process.stdout);
    //child.stderr.pipe(process.stderr);

    console.log("HEAT REDUCER SPAWNED");
    console.log("WORK ORDER SPAWNED");
    cronclock.init(true);
	
	
	// setInterval(function(){
	 // console.log("Auto dispatch test");
	// },4000);
	// setTimeout(function () {
        // console.log(db.getReservationData());
    // }, 10000);
	
};

/**
 * HANDLE INCOMING DATA
 * @param {Buffer} data
 * @param {exports.Client} client
 * @param {number} event
 */
module.exports.handleData = function(data, client, event)
{
    if(!process.debug) {
        try {
            //if not formatted
            if(!event)dataHandler.handleData(data, client);
            else dataHandler.handleEventMessage(event,data,client);
        }
        catch (e) {
            console.log(e.stack);
        }
    }
    else{
        dataHandler.handleData(data, client, event);
    }
};

/**
 * Handles client connect
 * @param {exports.Client} client
 */
module.exports.handleConnect = function(client)
{
    if(process.debug)console.log((new Date).toGMTString() +  " " +client.index.toString() + " Coneected");
};

/**
 * Handles client disconnect
 * @param {exports.Client} client
 */
module.exports.handleDisconnect = function(client)
{
    reconectHandler.delayDisconnect(client);
};

