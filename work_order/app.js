var SioClient, config, db, driverlist, events, framecut, getWorkorder, io, loggedIn, login, loginpanel, objectiveAcquired, panellist, prepareWorkorder, reportDone, self, setWorkorder, updatePosition;

SioClient = require("./../client_handler/client").SioClient;

framecut = require("framecut");

events = require("./../logic/client_lib/events.js");

events = events.requestEvents.workorder;

config = require("./../config");
var redis = require('socket.io-redis');
io = require("socket.io")(config.WORK_ORDER.SOCKET_IO_PORT);
io.adapter(redis({ host: 'localhost', port: 6379 }));

db = require("./../logic/database/database_handler");

var memored = require("memored");
console.log("WORK ORDER LISTENING ON " + config.WORK_ORDER.SOCKET_IO_PORT);

memored.store('wo_panellist', {});
memored.store('wo_driverlist', {})

// panellist = {};

// driverlist = {};

loggedIn = function(client) {
    return !!client && !!client.data && !!client.data.id;
};

self = module.exports;

io.on('connection', function(sock) {
    console.log('work order socket')
    var client;
    client = new SioClient(sock.handshake.address.address, sock.handshake.address.port, sock);
    console.log("Got new connection");
    sock.on(events.logindriver, function(data) {
        return login(data, client);
    });
    sock.on(events.position, function(data) {
        return updatePosition(data, client);
    });
    sock.on(events.objective, function(data) {
        return objectiveAcquired(data, client);
    });
    sock.on(events.loginpanel, function(data) {
        return loginpanel(data, client);
    });
    sock.on(events.workorder, function(data) {
        return setWorkorder(data, client);
    });
    sock.on(events.getworkorder, function(data) {
        return getWorkorder(data, client);
    });
    sock.on(events.done, function(data) {
        return reportDone(client);
    });
    return sock.on("disconnect", function() {
        var a, b, f, results;
        client.data = null;
        memored.read('wo_panellist', function(err, panellist) {
            f = function(a) {
                return delete panellist[a];
            };
            results = [];
            for (a in panellist) {
                b = panellist[a];
                if (b === sock) {
                    results.push(f(a));
                }
            }
            return results;
        })

    });
});

self.loginpanel = loginpanel = function(data, client) {
    console.log("loginpanel");
    memored.read('wo_panellist', function(err, panellist) {
        panellist[data] = client;
        memored.store('wo_panellist', panellist);
        return client.data = {
            id: data
        };
    })

};

self.getWorkorder = getWorkorder = function(data, client) {
    return db.getWorkorder(data, function(result) {
        return client.sendEvent(events.workorder, result);
    });
};

self.login = login = function(data, client) {
    var hashword, username;
    console.log("logging in " + data);
    username = data[0], hashword = data[1];
    return db.loginWorkorder(username, hashword, function(id) {
        if ((id != null) && id !== "") {
            return db.getWorkorder(id, function(result) {
                return db.getAcquiredObjs(id, function(objs) {
                    client.data = {
                        id: id,
                        acquiredObjs: []
                    };
                    memored.read('wo_driverlist', function(err, driverlist) {
                        driverlist[id] = client;
                        memored.store('wo_driverlist', driverlist)
                    })

                    client.sendEvent(events.success);
                    if (client.isSIO) {
                        return client.sendEvent(events.workorder, [result, objs]);
                    } else {
                        return client.sendEvent(events.workorder, prepareWorkorder(result, objs));
                    }
                });
            });
        } else {
            return client.sendEvent(events.failure);
        }
    });
};

self.updatePosition = updatePosition = function(data, client) {
    var lat, lng;
    console.log("position " + data);
    lat = data[0], lng = data[1];
    if (loggedIn(client)) {
        return db.workorderLogPosition(client.data.id, lat, lng);
    }
};

self.objectiveAcquired = objectiveAcquired = function(data, client) {
    var i, len, objId, panel, results;
    objId = data;
    if (loggedIn(client)) {
        client.data.acquiredObjs.push(objId);
        db.setAcquiredObjs(client.data.id, client.data.acquiredObjs);
        results = [];
        memored.read('wo_panellist', function(err, panellist) {
            for (i = 0, len = panellist.length; i < len; i++) {
                panel = panellist[i];
                results.push(panel.emit(events.objective, [client.data.id, objId]));
            }
        })
        return results;
    }
};

self.setWorkorder = setWorkorder = function(data, client) {
    var driverId, objectives;
    console.log(data);
    driverId = data[0], objectives = data[1];
    if (!driverId || !objectives) {
        return;
    }
    db.setWorkorder(driverId, objectives);
    memored.read('wo_driverlist', function(err, driverlist) {
        if (loggedIn(driverlist[driverId])) {
            if (driverlist[driverId].isSIO) {
                return driverlist[driverId].sendEvent(events.workorder, objectives);
            } else {
                return driverlist[driverId].sendEvent(events.workorder, prepareWorkorder(objectives));
            }
        }
    })

};

self.reportDone = reportDone = function(client) {
    var driverId;
    driverId = client.data.id;
    return db.getWorkorder(driverId, function(workorder) {
        if (!workorder.length === client.data.acquiredObjs.length) {
            return client.sendEvent(events.failure);
        } else {
            db.setWorkorder(driverId, []);
            db.setAcquiredObjs(driverId, []);
            return client.sendEvent(events.success);
        }
    });
};

prepareWorkorder = function(workorder, objectives) {
    var i, j, key, len, len1, obj, tmp, tmp1, val;
    console.log(workorder);
    tmp = "";
    tmp1 = "";
    if ((workorder == null) || workorder.length === 0) {
        return "";
    }
    for (i = 0, len = workorder.length; i < len; i++) {
        obj = workorder[i];
        tmp += ((function() {
            var results;
            results = [];
            for (key in obj) {
                val = obj[key];
                if (typeof val !== "object") {
                    results.push(val);
                }
            }
            return results;
        })()).join(" ") + "\n";
    }
    if ((objectives == null) || objectives.length === 0) {
        return tmp;
    }
    for (j = 0, len1 = objectives.length; j < len1; j++) {
        obj = objectives[j];
        tmp1 += ((function() {
            var results;
            results = [];
            for (key in obj) {
                val = obj[key];
                results.push(val);
            }
            return results;
        })()).join(" ") + "\n";
    }
    return [tmp, tmp1];
};

