/**
 * Created by root on 01/06/14.
 */
var driver_list = require("./../drivers/drivers_list");
var driver_sender = require("./../drivers/driver_sender");
var db          = require("./../../logic/database/database_handler");


driver_list.subscribe(function(data)
{
    console.log('============================')
    console.log('driver list debug')
    console.log(data);
    console.log('============================')
    if(!data.driver) return
    if(data.event == driver_list.events.message)
    {
        db.addMessage(data.driver.id, 0, data.msg);
    }
});

module.exports.sendMessage = function(operatorId, driverId, message)
{
    db.addMessage(parseInt(operatorId), parseInt(driverId), message);

    if(process.debug) console.log("Got message " + driverId + " " + message);
    var driver = driver_list.getDriver(driverId);
    console.log(driver)
    if(driver) driver_sender.sendMessage(driver, message);
};
module.exports.broadcast = function(operatorId, msg) {
    db.addMessage(parseInt(operatorId), "ALL", msg);

    if(process.debug) console.log("Got message TO ALL " + msg);
    var list = driver_list.getList();
    var driver;
    for(var id in list)
    {
        driver = list[id];
        if(driver.online && driver.onDuty) driver_sender.sendMessage(driver, msg);
    }
};