/**
 * Created by Chlopaki on 09.01.14.
 */
var memored = require("memored");
var driverList = require("./drivers/drivers_list");
var customerList = require("./clients/customer_list");


var config = require("./../config");

var reconnect_handler = {};
module.exports = reconnect_handler;

var http = require('http');
http.createServer(function(req, res) {});

// var list = {};
memored.store('rh_list', []);
var delay = config.RECONNECT_DELAY;
reconnect_handler.list = memored.read('rh_list', function(err, list) {
    return list;
})
reconnect_handler.checkRecentDisconnect = function(client, id) {
    var driver_handler = require("./drivers/driver_handler");
    var customer_handler = require("./clients/customer_handler");

    memored.read('rh_list', function(err, list) {
        if (list[id]) {
            if (list[id].client.driver) {
                client.driver = list[id];
                client.driver.client = client;

                client.driver.online = true;

                driver_handler.reconnectDriver(client.driver);
                if (process.debug) console.log(client.driver.reconnectTimeoutId);
                clearTimeout(client.driver.reconnectTimeoutId);
                if (process.debug) console.log("driver Reconnected");
                delete list[id];
                return true;
            }
            if (list[id].client.customer) {
                client.customer = list[id];
                list[id].client = client;
                client.customer.client = client;

                clearTimeout(client.customer.reconnectTimeoutId);
                if (process.debug) console.log("customer Reconnected");
                delete list[id];
                return true;
            }
        }
        console.log("no reconnect");
        return false;
    })




};
reconnect_handler.delayDisconnect = function(client) {
    //console.log("Delay disconnect")
    var target;
    var isDriver = !!client.driver;

    if (isDriver) {
        target = client.driver;
        driverList.disconnectDriver();
        target.online = false;

        var options = {
            host: 'localhost',
            port: 80,
            path: '/gcm_server_php/send_message1.php?Id=' + client.driver.id,
            method: 'GET'
        };

        http.request(options, function(res) {
            res.setEncoding('utf8');
            res.on('data', function(chunk) {
                console.log('BODY: ' + chunk);
            });
        }).end();

        console.log("Delay disconnect" + client.driver.id);
    } else if (client.customer) {
        target = client.customer;
    } else return;
    memored.read('rh_list', function(err, list){
        list[target.id] = target;
        memored.store('rh_list', list)
    })
    
    target.reconnectTimeoutId = setTimeout(function() {
        console.log("Disconneced and removed");
        //  var options = {
        //       host: 'localhost',
        //     port: 80,
        //   path: '/gcm_server_php/send_message1.php?Id=' + client.driver.id,
        // method: 'GET'
        //   };

        //http.request(options, function (res) {
        // res.setEncoding('utf8');
        //res.on('data', function (chunk) {
        //  console.log('BODY: ' + chunk);
        // });
        // }).end();
        if (isDriver) driverList.removeDriver(target.id);
        else customerList.removeCustomer(target.id);
        memored.read('rh_list', function(err, list){
            delete list[target.id];
            memored.store('rh_list', list)
        })
        
    }, delay)
};
reconnect_handler.setDelay = function(val) {
    delay = val;
};
