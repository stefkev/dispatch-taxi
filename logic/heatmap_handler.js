/**
 * Created by root on 10/06/14.
 */


var config = require("./../config");
var spawn   = require("child_process").spawn;
var weights = config.HEATMAP_WEIGHTS;

var db      = require('./database/database_handler');
var enabled = config.HEATMAP_ENABLED;

if(enabled) spawn("node" , ["./../heatmap/app/app.js"])

module.exports.requestSuccessful = function(lat, lng, jumps)
{
    if(enabled)db.increaseHeat(lat,lng, jumps*weights.ONE_JUMP);
    //TODO finish
};
module.exports.requestFailed = function(lat, lng)
{
    if(enabled)db.increaseHeat(lat,lng, weights.FAIL);
    //TODO finish
};
