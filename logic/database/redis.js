/**
 * Created by root on 13/05/14.
 */

var redis = require("redis");
var config = require("./../../config");

var CLIENTS_SET         = config.RED_CLIENTS_SET;
var DRIVERS_SET         = config.RED_DRIVERS_SET;
var LOG_LIST            = config.RED_LOG_LIST;
var RIDE_LIST           = config.RED_RIDE_LIST;
var FINISHED_RIDES      = config.RED_FINISHED_RIDES;
var DRIVERS_COUNT       = config.RED_DRIVERS_COUNT;
var FREE_DRIVERS_COUNT   = config.RED_FREE_DRIVERS_COUNT;
var DATE_LESSER         = new Date(2014,1,1);

var cli = redis.createClient();
console.log("REDIS STARTED");

exports.init = function()
{
    cli.del(DRIVERS_COUNT);
    cli.del(DRIVERS_COUNT);
    cli.del(FREE_DRIVERS_COUNT);
    cli.del(RIDE_LIST);

    return module.exports;
};

/**
 *
 * @param login
 * @param password
 * @param vehicle
 * @param id
 */

exports.registerCab = function(login, password, vehicle, id)
{
    cli.hset(DRIVERS_SET, login, JSON.stringify(
        {
            password: password,
            vehicle: vehicle,
            id: id
        }
    ))
};
/**
 *
 * @param login
 * @param password
 * @param cb
 */
exports.loginCab = function(login, password, cb)
{
    do_login(DRIVERS_SET, login, password,cb);
};

/**
 *
 * @param login
 * @param password
 */
exports.registerPassenger = function(login, password)
{
    cli.hset(CLIENTS_SET, login, JSON.stringify(
        {
            password: password
        }
    ))
};

/**
 *
 * @param login
 * @param password
 * @param cb
 */
exports.loginPassenger = function(login, password, cb)
{
    do_login(CLIENTS_SET, login, password,cb)
};

/**
 *
 * @param content
 */
exports.addLog = function(content)
{
    cli.zadd(LOG_LIST, Date.now()-DATE_LESSER, content);
};

/**
 *
 * @param date1
 * @param date2
 */
exports.getLogsByDateRange = function(date1, date2)
{
    cli.zrangebyscore(LOG_LIST, date1.getTime() - DATE_LESSER,
                                date2.getTime() - DATE_LESSER);
};

/**
 *
 * @param driverId
 * @param content
 */
exports.setRide = function(driverId, content)
{
    var tmp = JSON.stringify({
            stage: content.stage,
            rideId : content.index,
            from : content.from,
            to : content.to,
            additionalInfo: content.additionalInfo,
            fullname: content.fullname,
            phonenumber: content.phonenumber,
            dispatchtime:content.dispatchtime
        });

    cli.hset(RIDE_LIST, driverId, tmp);
};

/**
 *
 * @param driverId
 */
exports.removeRide = function(driverId)
{
    cli.hdel(RIDE_LIST, driverId, function(){});
};

exports.getRides   = function(cb)
{
    cli.hgetall(RIDE_LIST, cb);
};

/**
 *
 */
exports.incrementDriverCount = function()
{
    cli.incr(DRIVERS_COUNT);
    exports.incrementFreeDriverCount();
};

/**
 *
 */
exports.decrementDriverCount = function()
{
    cli.decr(DRIVERS_COUNT);
    exports.decrementFreeDriverCount();
};

exports.getDriversCount = function(cb)
{
    cli.get(DRIVERS_COUNT, cb);
};

/**
 *
 */
exports.incrementFreeDriverCount = function()
{
    cli.incr(FREE_DRIVERS_COUNT);
};

exports.increaseDriverStat = function(driverId)
{
    cli.zincrby(DRIVERS_STATS, 1, driverId);
};

exports.getDriverStats = function(cb)
{
    zrange(DRIVERS_STATS, 0, -1, "withscores", cb);
};

/**
 *
 */
exports.decrementFreeDriverCount = function()
{
    cli.decr(FREE_DRIVERS_COUNT);
};

exports.getFreeDriversCount = function(cb)
{
    cli.get(FREE_DRIVERS_COUNT, cb);
};

/**
 *
 */
exports.increaseFinishedRideCount = function()
{
    cli.incr(FINISHED_RIDES);
};
exports.getFinishedRidesCount = function(cb)
{
    cli.get(FINISHED_RIDES, cb)
};
exports.increaseHeat = function(lat, lng, heat){
    if(lat == undefined || lng == undefined) return;
    lat = parseFloat(lat);
    lng = parseFloat(lng);
    cli.hincrby(config.RED_HEATMAP, lat.toFixed(4) + '&' + lng.toFixed(4), heat, function(err,result)
    {
        if(err) console.warn(err);
    });
};
exports.logPosition = function (id , lat, lng)
{
    var b = new Buffer(8);

    lat = Math.round(lat*config.MULTIPLIER);
    lng = Math.round(lng*config.MULTIPLIER);

    b.writeInt32BE(lat, 0);
    b.writeInt32BE(lng, 4);
    cli.lpush("POSITION_" + id, Date.now() + "&" + b.toString());
};
exports.getPositionLog = function(id, from, to, cb)
{
    cli.lrange("POSITION_" + id, from, to, cb);
};

//TODO TO BE IMPLEMENTED
exports.reduceHeatBy = function(num, cb)
{
    return reduceHeat(num, cb)
};

exports.getHeat = function(cb)
{
    cli.hgetall(config.RED_HEATMAP, cb)
};

exports.getMessages = function(from, to, cb)
{
    cli.lrange(config.RED_MESSAGES_LIST, from, to, cb);
};
exports.addMessage = function(from, to , message)
{
    var time = Math.floor((new Date()).getTime()/1000);
    cli.lpush(config.RED_MESSAGES_LIST, JSON.stringify({from:from, to:to, time: time, message: message}));
};
exports.getLastChargeDate = function(cb)
{
    cli.get(config.RED_LAST_CHARGE_DATE, function(err, result)
    {
        var d = result ? result.split(".") : [0,0,0];
        cb(new Date(d[0],d[1],d[2]));
    })
};
exports.updateLastChargeDate = function()
{
    var date = new Date();
    var dateString = [date.getFullYear(), date.getMonth(), date.getDate()].join(".");
    cli.set(config.RED_LAST_CHARGE_DATE, dateString);
};

exports.getReservations             = function(customerId, callback)
{
    cli.hget(config.RED_RESERVATIONS_HASHMAP, customerId, function(err,res){ callback(JSON.parse(res)) } )
};
exports.setReservations             = function(customerId, reservations)
{
    console.log("setting reservations to ");
    console.log(reservations);
    cli.hset(config.RED_RESERVATIONS_HASHMAP, customerId, JSON.stringify(reservations))
};
exports.getAllReservations          = function(cb)
{
    cli.hgetall(config.RED_RESERVATIONS_HASHMAP, function(err, result){
        if(!result) return;
        cb(result)
    })
};

exports.getAcquiredObjs             = function(id, cb){
    cli.hget(config.RED_WORKORDER_ACQUIRED, id, function(err, res)
    {
        cb(JSON.parse(res))
    })
};
exports.setAcquiredObjs             = function(id, data){
    cli.hset(config.RED_WORKORDER_ACQUIRED,id, JSON.stringify(data))
};
exports.getWorkorder                = function(id, cb)
{
    cli.hget(config.RED_WORKORDER_ROUTE, id, function(err, res)
    {
        cb(JSON.parse(res))
    })
};
exports.setWorkorder                = function(id, data)
{
    cli.hset(config.RED_WORKORDER_ROUTE, id, JSON.stringify(data))
};
exports.remember                    = function(id, data)
{
    cli.hset(config.RED_REMEMBERED, id, JSON.stringify(data))
};
exports.getAllRemembered            = function(cb)
{
    cli.hgetall(config.RED_REMEMBERED, function(err, res){
        if(err){ cb(true, null); return}
        var tab  = [];
        for (var key in res) {
            tab.push(JSON.parse(res[key]))
        }
        cb(err, tab);
    })
};
exports.getPrices = function(cb)
{
    cli.get(config.RED_PRICES, function(err, res) {
        if(err){ cb(true, null) ; return}
        cb(err, res)
    })
};
exports.setPrices = function(data)
{
    cli.set(config.RED_PRICES, data)
}
;
//--------------------------------------------------------------------------//
//--------------------------------PRIVATE-----------------------------------//
//--------------------------------------------------------------------------//


function reduceHeat(num, cb)
{
    cli.hgetall(config.RED_HEATMAP, function(err, keyval)
   {
       var result = "";
       if(!err) {
           for(var key in keyval) {
               if (keyval.hasOwnProperty(key)) {

                   var val = parseInt(keyval[key])*(num/100);
                   result += key + " => " + val + "\n";

                   if(val > 0.2)cli.hset(config.RED_HEATMAP, key, val);
                   else cli.hdel(config.RED_HEATMAP, key);
               }
           }

       }
       cb(result);
   })
}
function do_login(table, login, password, cb)
{
    cli.hget(table, login, function(err, res)
    {
        res = JSON.parse(res);
        if(err) throw err;
        else {
            if(res == null) cb(false, null, null);
            else {
                if(res.password == password) cb(true, true, res);
                else cb(true, false, null)
            }
        }
    });
}



//DEBUG
exports.registerPassenger("test","test");
exports.registerCab("test","test","test", 300);
