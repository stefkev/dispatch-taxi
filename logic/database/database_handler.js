/**
 * Created by Iraasta on 22.12.13.
 */
var config = require("./../../config");

var db_module       = require("./" + config.DB_MODULE).init();
var db_auth_module  = require("./" + config.DB_AUTH_MODULE).init();

exports.loginCab                    = db_auth_module.loginCab;
exports.loginPassenger              = db_auth_module.loginPassenger;
exports.registerClient              = db_auth_module.registerClient;

exports.addLog                      = db_module.addLog;
exports.getLogsByDateRange          = db_module.getLogsByDateRange;

exports.setRide                     = db_module.setRide;
exports.removeRide                  = db_module.removeRide;
exports.getRides                    = db_module.getRides;

exports.incrementDriverCount        = db_module.incrementDriverCount;
exports.decrementDriverCount        = db_module.decrementDriverCount;
exports.getDriversCount             = db_module.getDriversCount;

exports.incrementFreeDriverCount    = db_module.incrementFreeDriverCount;
exports.decrementFreeDriverCount    = db_module.decrementFreeDriverCount;
exports.getFreeDriversCount         = db_module.getFreeDriversCount;

exports.increaseFinishedRideCount   = db_module.increaseFinishedRideCount;
exports.getFinishedRidesCount       = db_module.getFinishedRidesCount;
exports.logPosition                 = db_auth_module.logPosition;
exports.getPositionLog              = db_auth_module.getPositionLog;

exports.increaseHeat                = db_module.increaseHeat;
exports.reduceHeatBy                = db_module.reduceHeatBy;

exports.getHeat                     = db_module.getHeat;

exports.addMessage                  = db_module.addMessage;
exports.getMessages                 = db_module.getMessages;

exports.reportEvent                 = db_auth_module.reportEvent;
exports.chargeAllAccounts           = db_auth_module.chargeAllAccounts;
exports.getLastChargeDate           = db_module.getLastChargeDate;
exports.updateLastChargeDate        = db_module.updateLastChargeDate;

exports.getReservations             = db_module.getReservations;
exports.setReservations             = db_module.setReservations;
exports.getAllReservations          = db_module.getAllReservations;

exports.workorderLogPosition        = db_auth_module.workorderLogPosition;
exports.loginWorkorder              = db_auth_module.loginWorkorder;
exports.getAcquiredObjs             = db_module.getAcquiredObjs;
exports.setAcquiredObjs             = db_module.setAcquiredObjs;
exports.setWorkorder                = db_module.setWorkorder;
exports.getWorkorder                = db_module.getWorkorder;
exports.remember                    = db_module.remember;
exports.getAllRemembered            = db_module.getAllRemembered;
exports.getPrices                   = db_module.getPrices;
exports.setPrices                   = db_module.setPrices;

exports.getReservationData          = db_auth_module.getReservationData;
exports.getZoneData				    = db_auth_module.getZoneData;

function hash(val)
{
    var crypto      = require('crypto');
    return crypto.createHash(config.HASH_ALGORITHM).digest(val).toJSON().join("");
}
