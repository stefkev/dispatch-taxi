/**
 * Created by root on 13/05/14.
 */

var config = require("./../../config");
var dbconfig =
{
    host: config.SQL_HOST,
    user: config.SQL_USER,
    password: config.SQL_PASSWORD,
    database: config.SQL_DATABASE
};

var mysql       = require('mysql');
var crypto      = require('crypto');


/**
 * REFACTOR
 */
var pool = mysql.createPool(dbconfig);

var connection = {
    query: function () {
        var queryArgs = Array.prototype.slice.call(arguments),
            events = [],
            eventNameIndex = {};

        pool.getConnection(function (err, conn) {
            if (err) {
                if (eventNameIndex.error) {
                    eventNameIndex.error();
                }
            }
            if (conn) {
                var q = conn.query.apply(conn, queryArgs);
                q.on('end', function () {
                    conn.release();
                });

                events.forEach(function (args) {
                    q.on.apply(q, args);
                });
            }
        });

        return {
            on: function (eventName, callback) {
                events.push(Array.prototype.slice.call(arguments));
                eventNameIndex[eventName] = callback;
                return this;
            }
        };
    }
};
/**
 *
 */

/*function handleDisconnect() {
    connection = mysql.createConnection(dbconfig); // Recreate the connection, since
    // the old one cannot be reused.

    connection.connect(function(err) {              // The server is either down
        if(err) {                                     // or restarting (takes a while sometimes).
            console.log('error when connecting to db:', err);
            setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
        }
        else console.log("MYSQL: OK");
                                             // to avoid a hot loop, and to allow our node script to
    });                                     // process asynchronous requests in the meantime.
    // If you're also serving http, display a 503 error.
    connection.on('error', function(err) {
        console.log('db error', err);
        if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
            handleDisconnect();                         // lost due to either server restart, or a
        } else {                                      // connnection idle timeout (the wait_timeout
            throw err;                                  // server variable configures this)
        }
    });
}
handleDisconnect(); */

var databaseHandler = {};
module.exports = databaseHandler;
var CLIENTS_TABLE = config.SQL_CLIENTS_TABLE;
var DRIVERS_TABLE = config.SQL_DRIVERS_TABLE;

//INTERFACE
databaseHandler.loginCab        = loginCab;
databaseHandler.loginPassenger  = loginPassenger;

/**
 * FACADE PATTERN
 * @param {string} tableName
 * @param {string} [select]
 * @param {string} [where]
 * @param {function} cb
 */
databaseHandler.select = function(tableName,select,where,cb)
{

    if(!tableName || !cb )throw  Error("tableName and callback must be defined");
    if(!select) select = "*";

    if(!!where)
    {
        connection.query('SELECT ' + select + ' FROM '+ tableName + ' WHERE ' + where, function(err, result) {
            if(err)console.log(err);
            else{
                cb(result);
            }
        });
    }
    else
    {
        connection.query('SELECT ' + select + ' FROM '+ tableName, function(err, result) {
            if(err){
                console.log(err);
            }
            else{
                // console.log(result)
                cb(result);
            }
        });
    }
};
/**
 *
 * @param {string} tableName
 * @param {object} values
 * @param {function} [cb]
 */
databaseHandler.insert = function(tableName,values, cb)
{
    connection.query('INSERT INTO '+ tableName + ' SET ?', values, function(err, result) {
        if(err)console.warn("error in databse insert" + err.toString());
        else{
            if(cb){
                cb(null,result.insertId);
            }
        }
    });
};


/**
 * check if exists in db
 * cb = function(boolean)
 * @param tableName
 * @param where
 * @param cb
 */
databaseHandler.exists = function(tableName,where,cb)
{
    databaseHandler.select(tableName,null,where,function(result)
    {
        cb(result.length != 0);
    });
};

databaseHandler.init = function()
{
    console.log("MySQl driver started");
    return module.exports;
};

databaseHandler.logPosition = function(id, lat, lng) {
    lat = Math.round(lat * config.MULTIPLIER);
    lng = Math.round(lng * config.MULTIPLIER);

    var values = {
        driver_id: id,
        lat: lat,
        long: lng,
        time: Math.floor((new Date).getTime()/1000)};
    databaseHandler.insert(config.SQL_POSTION_LOG, values, function () {
    });
};

databaseHandler.reportEvent = function(event, id, additional)
{
    databaseHandler.insert(config.SQL_EVENT_LOG, {id: id, event: event, additional: additional, time:getTime()})
};

databaseHandler.getPositionLog = function(id, from, to, cb)
{
    //TODO databaseHandler.select(config.SQL_POSTION_LOG, )
};

databaseHandler.registerClient = function(name, login, password, email, phonenumber, cb)
{
    var query = "login = \'" + login + "\' OR emailaddress = \'" + email + "\'";
    console.log(query);
    databaseHandler.select(config.SQL_CLIENTS_TABLE, "*", query,
        function(result)
        {
            if(result.length > 0)
            {
                cb(new Error("Login or email already taken"))
            }
            else
            {
                var values = {fullname: name,
                            login: login,
                            password: password,
                            emailaddress: email,
                            phonenumber: phonenumber
                };
               databaseHandler.insert(config.SQL_CLIENTS_TABLE, values, function(err, index)
               {
                   cb(null)
               });
            }
        })
};
databaseHandler.chargeAllAccounts = function()
{
    connection.query("UPDATE "+ config.SQL_DRIVERS_TABLE + " SET due = drivers.due + drivers.CPW");
    console.log("DRIVERS CHARGED")
};

databaseHandler.workorderLogPosition        = function(id, lat, lng)
{
    lat = Math.round(lat * config.MULTIPLIER);
    lng = Math.round(lng * config.MULTIPLIER);

    var values = {
        driver_id: id,
        lat: lat,
        lng: lng,
        timestamp: Math.floor((new Date).getTime()/1000)};
    databaseHandler.insert(config.SQL_WORKORDER_POSITIONS, values, function () {});
};
databaseHandler.loginWorkorder              = function(login, hashword, cb)
{
    databaseHandler.select(config.SQL_WORKORDER_LOGINS,null,"emailaddress = '" + login +"'", function(result){
        if(result.length && (result[0].password = hashword)) cb(result[0].id)
        else cb(null)
    });
}

databaseHandler.getZoneData = function(cb)
{
    databaseHandler.select("zones", " Zone_name,latitude,longitude ", null, function (result) {
        // console.log(result)
        cb(result);
    });
};

function getTime()
{
    return Math.round(Date.now()/1000);
}

function loginCab(login, password, cb)
{
    // login exists. passwordMatches, id, vehicle
    
    console.log("Login Attempt by:" + login + "pass:" +password);
    databaseHandler.select(DRIVERS_TABLE,null,"emailaddress = '" + login +"'", function(result){
        if(result.length == 0)
        {
            cb(true, null, null);
        }
        else{
            if(result[0].password == password)
            {
                console.log("Login Sucess :" + login);
                cb(true, true, result[0]);
            }
            else
            {
                cb(true, false, null)
            }
        }
    });
}
function loginPassenger(login, password, cb)
{
    databaseHandler.select(CLIENTS_TABLE,null,"login = '" + login +"'", function(result){
        if(result.length != 1)
        {
            cb(false, null, null)
        }
        else{
            if(result[0].password == password)
            {
                cb(true,true, result[0])
            }
            else
            {
                cb(true,false, null)
            }
        }
    });
}

function getReservationData() {

    databaseHandler.select("reservationexcel", null, null, function (result) {
        return result;
    });

}


