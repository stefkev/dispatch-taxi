config = require "./../../config"
cronclock = require "cronclock"
db = require "./../database/database_handler"
fs = require "fs"
path = require "path"

DAY_OF_CHARGE = config.CHARGE_DAY

chargeAllAccounts = ()->
  db.chargeAllAccounts();
  fs.appendFileSync(path.join(__dirname,"charge_log.txt"), (new Date).toGMTString() + " CHARGED \n")
  db.updateLastChargeDate()

decorateTimeSpan = (milliseconds)->
  seconds = Math.floor(milliseconds/1000)%60
  minutes = Math.floor(milliseconds/(1000*60))%60
  hours   = Math.floor(milliseconds/(1000*60*60))%24
  days    = Math.floor(milliseconds/(1000*60*60*24))

  "#{days} Days #{hours} Hours #{minutes} Minutes and #{seconds} seconds"

db.getLastChargeDate((date) ->
  if(date.getTime() == (new Date(0,0,0)).getTime())
    cronclock.add(new Date, chargeAllAccounts, cronclock.REPEAT_EVERY.week)
    return

  # if it was today
  today = new Date()
  if((new Date(date.getFullYear() , date.getMonth() , date.getDate())).getTime() == (new Date(today.getFullYear(), today.getMonth(), today.getDate())).getTime())
    console.log("ALREADY CHARGED TODAY")
    newDate = new Date(Date.now() + 1000*60*60*24*7);
    cronclock.add(newDate, chargeAllAccounts, cronclock.REPEAT_EVERY.week)
    console.log "Accounts will be charged in #{decorateTimeSpan(newDate.getTime() - Date.now())}"
    return

  #next week
  diffInDays = ((DAY_OF_CHARGE + 7) - (new Date).getDay()) % 7;
  newDate = new Date(date.getTime() + diffInDays * 24 * 60 * 60 * 1000)

  # 01:00:00 For day border errors
  newDate.setHours(1)
  newDate.setMinutes(0)
  newDate.setSeconds(0)

  cronclock.add(newDate, chargeAllAccounts, cronclock.REPEAT_EVERY.week)

  console.log "Accounts will be charged in #{decorateTimeSpan(newDate.getTime() - Date.now())}"
)
