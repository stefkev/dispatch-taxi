/**
 * Created by Iraasta on 18.12.13.
 */
//-----------------------------------IMPORTS------------------------------
var workorder       = require("./../work_order/app.js");
var events          = require("./client_lib/events").requestEvents;
var resDEvents       = require("./client_lib/events").responseEvents;
var resCEvents       = require("./client_lib/events").clientReponseEvents;
var driverHandler   = require("./drivers/driver_handler.js");
var clientHandler   = require("./clients/customer_handler.js");
var security        = require("./security/packet_security.js");
var buffer_coder    = require("./buffer_coder.js");
var bugHandler      = require("./bug_handler");
var db              = require("./database/database_handler.js");
var MULTIPLIER      = require("./../config").MULTIPLIER;
var reservations    = require("./reservations/reservation_handler.js");
var smsSender       = require("./../services/sms.coffee");
var driverList      = require("./drivers/drivers_list.js");


var reportableEvents = [
    events.acceptRequest, // 4,
    events.rejectRequest, // 5
    events.clientPickUp,  // 9
    events.clientDropOff, // 10
    events.customerLogin, // 11
    events.loginCab,      // 2
    events.emergencyCancel // 30

];
var t = buffer_coder.type;
//-----------------------------------END----------------------------------

/**
 *
 * @param {Buffer} buff
 * @param {exports.Client} client
 * @param [ev]
 */
module.exports.handleData = function(buff,client, ev)
{
    var type;
    var data;
    if(Buffer.isBuffer(buff)) {
        type = parseInt(buff[0]);
        data = new Buffer(buff.length - 1);
        buff.copy(data, 0, 1);
    }
    else
    {
        type = ev;
        data = buff
    }

    if(process.debug)
    {
        var eventName = "";
        for(var name in events)
        {
            if(events.hasOwnProperty(name))
            {
                if(events[name] == type)
                {
                    eventName = name;
                    break;
                }
            }
        }
        //console.log("EVENT: " + eventName +"   DATA: " + data.toString());
    }

    module.exports.handleEventMessage(type,data,client)
};
module.exports.handleEventMessage = function(event, data, client)
{
    if( reportableEvents.indexOf(event) != -1)
    {
        var id = client.driver ? client.driver.id : client.customer? client.customer.id : -1;
        if(id != -1 ) {
		
            if(event != events.clientDropOff) 
			{ 
			  //console.log('Data:'+ data.toString());
			  db.reportEvent(event, id, data.toString());
			}
            else{
			console.log('Client Data:'+ client.driver.ride.customer);
                var riderep = prepareRideReport(client);
				console.log('Ride:'+ riderep.toString());
                if(riderep) db.reportEvent(event, id, riderep)
            }
        }
    }
//console.log(event);
    switch (event)
    {
		
        case events.registerClient:
            registerClient(data, client);
            break;
        case events.loginCab:
            loginCab(data,client);
            break;
        case events.logoutCab:
            logoutCab(data,client);
            break;
        case events.updatePosition:
            updatePosition(data, client);
            break;
        case events.lookForCab:
            lookForCab(data,client);
            break;
        case events.makeRequest:
            makeRequest(data,client);
            break;
	    case events.makeRadioRequest:
            makeRadioRequest(data,client);
            break;
        case events.acceptRequest:
            handleRequest(true, data, client);
            break;
        case events.rejectRequest:
            handleRequest(false, data, client);
            break;
        case events.requestCancelled:
            requestCancelled(data,client);
            break;
        case events.clientPickUp:
            clientPickUp(data,client);
            break;
        case events.clientDropOff:
            clientDropOff(data,client);
            break;
        case events.customerLogin:
            customerLogin(data,client);
            break;
        case events.bugReport:
            bugReport(data);
            break;
        case events.messageSend:
            receiveMessage(data,client);
            break;
        case events.getHeatmap:
            getHeatmap(data, client);
            break;
        case events.getPositionLog:
            getPositionLog(data, client);
            break;
        case events.cancelReservation:
            cancelReservation(data, client);
            break;
        case events.reportDriverState:
            reportDriverState(data, client);
            break;
        case events.reportPassengerState:
            reportPassengerState(data, client);
            break;
        case events.changeDuty:

            if(client.driver)client.driver.onDuty = !!data.toString();
            break;
        case events.sendSms:
            if(client.driver) sendSms(client.driver);
            break;

        //Workorder events
        case events.workorder.logindriver:      workorder.login(data.toString().split("|"),client);            break;
        case events.workorder.position:         workorder.updatePosition(data.toString().split("|"),client);   break;
        case events.workorder.objective:        workorder.objectiveAcquired(data,client);           break;
        case events.workorder.getworkorder:     workorder.getWorkorder(data, client);               break;
        case events.workorder.done:             workorder.reportDone(client);                       break;


        default :
            console.log("packet error with event " + event);
            break;
    }
};
function reportDriverState(data, client){
    client.driver.ride.state = data;
    console.log("driver state set to " + client.driver.ride.state);
}
function reportPassengerState (data, client) {
    console.log("deprecated passenger state");
}
function registerClient(data, client)
{
    var cake = data.toString().split('|');

    //check length of the packet and return error if wrong
    if(!security.checkPacketLength(5,cake))return;

                                // client, name, login, password, email, phonenum
    clientHandler.registerClient(client, cake[0],cake[1],cake[2],cake[3], cake[4]);
}
function loginCab(data,client)
{
 console.log(data);
    var cake = data.toString().split('|');
	if(cake[2] == undefined){cake[2] = 0;}

    //check length of the packet and return error if wrong
    if(!security.checkPacketLength(3,cake))return;
    if(security.checkIfLoggedIn(client))return;

    driverHandler.loginCab(client, cake[0],cake[1],cake[2], function()
    {
        if(!client.driver.ride) return;
        console.log("Sending driver state " + client.driver.ride.getState());
        client.sendEvent(resDEvents.stateReport, client.driver.ride.getState() || "")
    })
}
function logoutCab(data,client)
{
    driverHandler.logoutCab(client);
}
function updatePosition(data, client)
{
    var packetTemp = [t.int32, t.int32];
    if(!security.checkPacketLength(buffer_coder.getPacketLength(packetTemp),data))return;

    var values = buffer_coder.decodeByte(data, packetTemp );
    if(values[0] == 0 || values[1] == 0) return;
    driverHandler.updatePosition(values[0],values[1],client);
}
function lookForCab(data,client)
{
    var values = data.toString().split("|");
    if(!security.checkPacketLength(1,values))return;

    //if(!security.checkIfLoggedIn(client))
    //{
    //    client.sendEvent(resEvents.notLoggedIn);
    //    return;
    //}
                            //name   , latitude          ,  longitude         , destination
                            //    , additionalInfo
    if(client.customer)
    {
        try{
            client.customer.latitude = parseInt(data.readInt32BE(0))/MULTIPLIER;
            client.customer.longitude = parseInt(data.readInt32BE(4))/MULTIPLIER;
            clientHandler.lookForCab(client.customer, values[3]) ;
        }
        catch(e)
        {
            console.log("wrong data in lookForCab" + e.toString() + e.stack);
        }
    }
    else
    {
        try{

            var customer = {};
            customer.latitude = parseInt(data.readInt32BE(0))/MULTIPLIER;
            customer.longitude = parseInt(data.readInt32BE(4))/MULTIPLIER;
            customer.client = client;
            clientHandler.lookForCab(customer, values[3]) ;
        }
        catch(e)
        {
            console.log("wrong data in lookForCab"+ e.toString() + e.stack);
        }
    }

}
function handleRequest(accepted, data, client)
{
    data = data.toString();
    var values = data != "" ? data.split("|") : [];
    if(!security.checkPacketLength(1,values))return;

    console.log("ride accepted: " + accepted);
    if(accepted)driverHandler.acceptRide(values[0],client);
    else driverHandler.rejectRide(values[0],client);
}
function makeRequest (data, client)
{
    var values = data.toString().split("|");
 
 var monthAndDay, time, date, minutesBefore;
    if(parseInt(values[0]) == -1)
    {
        monthAndDay = values[6].split(" ")[0];
        time        = values[6].split(" ")[1];
        date = new Date((new Date()).getFullYear(),
            monthAndDay.split(".")[1],
            monthAndDay.split(".")[0],
            time.split(":")[0],
            time.split(":")[1]);
        if (date.getTime() < Date.now()) date.setFullYear(date.getFullYear()+1); 
        minutesBefore = parseInt(values[7])
    }
    else date = minutesBefore = null;
	console.log("Request Ride customer details : "+client.customer);
    //clientHandler.requestRide(//id,   lat,      long,    fullname,   dest,    vrhicle,    ,date, minutesB, client, additional)
    clientHandler.requestRide(values[0],values[1],values[2],values[3],values[4], values[5], date, minutesBefore ,client, values[8], null);
    // for (var i = 0; i < driverList.length; i++) {
        // clientHandler.requestRide(driverList[i].driverId, values[1], values[2], values[3], values[4], values[5], date, minutesBefore, client, values[8], null);
    // } 
}
function makeRadioRequest(data, client) {
   
    
    // this.from = [23.022505, 72.57136209999999];
    // this.to = ["23.2156354","72.63694149999992"];
    // this.stage = 1;
    // this.index = 0;

    //db.setRide(695, this);
    //"{\"stage\":1,\"rideId\":0,\"from\":[23.022505,72.57136209999999],\"to\":[\"23.2156354\",\"72.63694149999992\"]}"
    db.reportEvent(4, 695, "1");
}

function requestCancelled (data, client)
{
    data = data.toString();
    var values = data != "" ? data.split("|") : [];
    if(!security.checkPacketLength(1,values))return;
    clientHandler.cancelRequest(values[0],client, true);
}
function clientPickUp (data,client)
{
    var rideId = data.toString();
    driverHandler.clientPickUp(client);
}
function clientDropOff (data,client)
{
    var rideId = data.toString();
    driverHandler.clientDropOff(client);
}
function customerLogin(data,client)
{
    var values = data.toString().split("|");

    if(security.checkIfLoggedIn(client))return;

    clientHandler.login(values[0],values[1],client, function()
    {
        if(!client.customer.ride) return;

        console.log("Sending client state " + client.customer.ride.getState());
        client.sendEvent(resCEvents.stateReport, client.customer.ride.getState() || "")
    })
}
function receiveMessage(data, client)
{
    driverHandler.receiveMessage(data,client);
}

function getHeatmap(data, client)
{
    //TODO getting heatmap for a specified area to minimize load
    driverHandler.getHeatmap(client);
}

function getPositionLog(data, client)
{
    var cake = data.toString().split("|");
    db.getPositionLog(cake[0],cake[1],cake[2], function(err, result)
    {
        if(!err) client.sendEvent(resDEvents.positionLog, result)
    })
}

function cancelReservation(data, client)
{
    reservations.cancelReservation(client.customer.id, data.toString(), function(success)
    {
        console.log("Success = " + success)
        if(success)client.sendEvent(resCEvents.reservationCancelSuccess, [data.toString()])
        else client.sendEvent(resCEvents.reservationCancelFailure, [data.toString()])
    })
}
function changeDuty(data, client)
{
    driverList.changeDriverDuty(data.driver, data == "1");
}

function bugReport(data)
{
    bugHandler.report(data.toString());
}

function prepareRideReport(client)
{
    if(!client.driver.ride || !client.driver.ride.from || !client.driver.ride.to) return null;
    return [client.driver.ride.from.join("|"),
            client.driver.ride.to.join("|"),
            client.driver.ride.customer.id,
            client.driver.ride.customer.phonenumber
    ].join("|")
}
function sendSms(driver)
{
    if(driver.ride && driver.ride.customer && driver.ride.customer.phonenumber)
    {
        if( process.debug)console.log(driver.ride.customer.phonenumber);
        console.log("Sening sms on " + driver.ride.customer.phonenumber);
        smsSender.send(driver.ride.customer.phonenumber, "Car outside", config.SMS_CONTENT, function(){});
        driverList.smsSent(driver.id)
    }
}