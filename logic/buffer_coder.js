/**
 * Created by Chlopaki on 20.12.13.
 */

/**
 * int24 EXPERIMENTAL
 * @type {{byte: number, int16: number, int24: number, int32: number, int64: number, long: number}}
 */
var type = {
    byte : 1,
    int16: 2,
    int24: 3,
    int32: 4,
    int64: 8,
    long: 8
};
module.exports.type = {
    byte : 1,
    int16: 2,
    int24: 3,
    int32: 4,
    int64: 8,
    long: 8
};

/**
 * Types template [byte,byte,int32,int32,long]
 * returns [[x],[x],[x,x,x,x],[x,x,x,x],[x,x,x,x,x,x,x,x]]
 * @param {Buffer} data
 * @param {Array} types
 * @returns {Array}
 */
module.exports.decodeByte = function(data, types)
{
    var len = types.length;
    var returnTab = [];
    var progress = 0;
    for(var i = 0 ; i < len; i++)
    {
        switch (types[i])
        {

            case type.byte:
                returnTab.push(data[progress]);
                break;
            case type.int32:
                returnTab.push(data.readInt32BE(progress));
                break;

            case type.int16:
                throw Error("NOT IMPLEMENTED YET");
                break;
            case type.int24:
                throw Error("NOT IMPLEMENTED YET");
                break;
            case type.int64:
		returnTab.push(data.readInt64BE(progress));
                break;
        }
        progress+= types[i];
    }
    return returnTab;
};
/**
 * Types template [byte,byte,int32,int32,long]
 * returns [[x],[x],[x,x,x,x],[x,x,x,x],[x,x,x,x,x,x,x,x]]
 * @param {Array} data
 * @param {Array} types
 * @returns {Buffer}
 */
module.exports.encodeByte = function(data, types)
{
    var len = types.length;
    var byteLen = module.exports.getPacketLength(types);
    var buf = new Buffer(byteLen);
    var progress = 0;
    for(var i = 0 ; i < len; i++)
    {
        switch (types[i])
        {
            case type.byte:
                buf.writeInt8(data[i],progress);
                break;
            case type.int32:
                buf.writeUInt32BE(data[i],progress);
                break;

            case type.int16:
                throw Error("NOT IMPLEMENTED YET");
                break;
            case type.int24:
                throw Error("NOT IMPLEMENTED YET");
                break;
            case type.int64:
		buf.writeInt64BE(data[i],progress);
                break;
        }
        progress+= types[i];
    }
    return buf;
};
module.exports.getPacketLength = function(types)
{
    var byteLen = 0;
    for(var i = 0 ; i < types.length; i++)
    {
        byteLen+=types[i];
    }
    return byteLen;
};
