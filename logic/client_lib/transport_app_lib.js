/**
 * Created by root on 13.07.14.
 */
(function () {
    const ADDRESS = "104.131.110.50";
    const PARAMETER_NAME = "key";
    const PORT = 9000;

    window.cloudcab = {};
    var observers = {};
    var socket;

    window.cloudcab.init = function (apiKey, callback) {
        makeReq(ADDRESS + "?" + PARAMETER_NAME + "=" + apiKey, function (result) {
            console.log(result);
            var config = JSON.parse(result);
            socket = io.connect("http://" + config.ip + ":" + config.port);
            callback();
        })
    };

    function makeReq(address, cb) {
        //TODO HTTPREQUEST IP:81/apikey?key=apiKey
        cb(JSON.stringify({ip:ADDRESS, port:PORT}));
        /*$.ajax({
            type: "GET",
            url: address
        }).done(cb);*/
    }

    function notify(event, data) {
        if (observers[event]) {
            for (var fun in observers[event]) {
                if (observers.hasOwnProperty(fun))fun(data);
            }
        }
    }

    //event receiving
    cloudcab.on = function (event, cb) {
        sock.on(event, cb);
    };

    //event sending
    cloudcab.sendEvent = function (event, data) {
        sock.emit(event, data.join("|"));
    };
})();