// !!! BROWSER COMPATIBILITY TRICK. ANTI-PATTERN !!!
module = !module.exports ? {exports: window} : module;
//----------------------------------------

/**
 * Created by Iraasta on 18.12.13.
 */
module.exports.requestEvents = {
    lookForCab: 0,          // eventID |  lat | long | (not yet) type of vehicle
    registerClient: 1,         // OUT OF USE
    loginCab: 2,            // login | password
    logoutCab: 3,            // {empty}
    acceptRequest: 4,        // rideID
    rejectRequest: 5,        // rideId
    requestCancelled: 6,    // driverId
    makeRequest: 7,        // driverId |  lat | long | fullname | dest | jump | additionalInfo

    updatePosition: 8,       // lat (4Bytes), long(4bytes)
    clientPickUp: 9,        // {empty}
    clientDropOff: 10,       // {empty

    customerLogin: 11,       //login | password    messageSend: 11,         //
    messageSend: 12,         //
    getHeatmap: 13,
    getPositionLog: 14,      // ID | FROM | TO &NOT IN USE YET
    cancelReservation: 15,   // ID |

    workorder: {
        logindriver: 16,
        position: 17,
        objective: 18,
        loginpanel: 19,
        workorder: 20,
        getworkorder: 21,
        done: 22,
        success: 23,
        failure: 24
    },
    changeDuty : 25,
    reportDriverState: 26,
    reportPassengerState: 27,
    getDriverState: 28,
    getPassangerState: 29,
    emergencyCancel : 30,
    sendSms : 31,

    bugReport: 69
};
module.exports.responseEvents = {
    sendId: 0,             // deprecated
    requestRide: 1,         // id, lat, long, customer.destination, customer.additionalInfo
    driverAlreadyExists: 2, // {empty}
    noSuchUser: 3,          // {empty}
    loginFailed: 4,         // {empty}
    loginSuccessful: 5,     // id
    requestCancelled: 6,    // rideId
    actualRideCancelled: 7, // {empty}
    notLoggedIn: 8,

    reconnection: 9,
    sendMessage: 10,
    heatmapData: 11,
    endHeatmap: 12,
    positionLog: 13,
    stateReport : 14,
    emergencyCancel: 15,

    somethingWentWrong: 69 //
};
module.exports.clientReponseEvents =
{
    sendId: 0,             // id
    noSuchUser: 3,          // {empty}
    loginFailed: 4,         // {empty}
    loginSuccessful: 5,     // id
    registrationFailed: 6,
    registrationSuccess: 7,
    notLoggedIn: 8,
    reservationSuccess: 9,
    reservationsData: 10,
    reservationCancelSuccess: 11,
    reservationCancelFailure: 12,
    stateReport : 14,
    emergencyCancel: 15,

    noDriverFound: 52,      // {empty}
    driverFound: 53,
    waitingForDriver: 54,   // driverId, distance, vehicle
    rideRejected: 55,        // {empty}
    rideAccepted: 56,        // {empty}
    driverMoved: 57,
    pickupConfirmed: 58,
    dropoffConfirmed: 59,

    reconnection: 60,

    somethingWentWrong: 69  // {empty}

};

var control = [];
var tab = module.exports.responseEvents;

for (var i in tab) {
    if (control.indexOf(tab[i]) != -1)throw new Error("EVENT IDS!!!!");
    control.push(tab[i]);
}
control = [];
tab = module.exports.clientReponseEvents;
for (i in tab) {
    if (control.indexOf(tab[i]) != -1)throw new Error("EVENT IDS!!!!");
    control.push(tab[i]);
}

for (var i = 0; i < module.exports.responseEvents; i++) {
    if (control.indexOf(module.exports.responseEvents[i] != -1))throw new Error("EVENT IDS!!!!");
}
control = [];
for (i = 0; i < module.exports.clientReponseEvents; i++) {
    if (control.indexOf(module.exports.clientReponseEvents[i] != -1))throw new Error("EVENT IDS!!!!");
}
control = [];
for (i = 0; i < module.exports.requestEvents; i++) {
    if (control.indexOf(module.exports.requestEvents[i] != -1))throw new Error("EVENT IDS!!!!");
}
tab = module.exports.requestEvents;
for (i in tab) {
    if (control.indexOf(tab[i]) != -1)throw new Error("EVENT IDS!!!!");
    control.push(tab[i]);
}