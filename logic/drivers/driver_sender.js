/**
 * Created by Iraasta on 22.12.13.
 */;
var bufferCoder = require("./../buffer_coder");
var resEvents      = require("./../client_lib/events").responseEvents;
var t = bufferCoder.type;
var config = require("./../../config");

var self = {};
module.exports = self;

var buff;

/**
 *
 * @param  client
 * @param {number} id
 */
self.sendId = function(client,id)
{
    try{
    buff = bufferCoder.encodeByte([resEvents.sendId,id],[t.byte,bufferCoder.type.int32]);
        client.send(buff);
    }
    catch(err)
    {
        console.log(err);
    }
};
self.sendRideRequest = function(driver, lat, long, vehicle, customer, uniqueId)
{
    if(driver == null)
    {
        console.log("DRIVER NULL");
        return false;
    }
    if(driver.ride || !driver.onDuty || !driver.online || !matchVehicles(driver.vehicle, vehicle))
    {
        console.log("Driver already has a ride " + [driver.ride, driver.onDuty, driver.online, [vehicle, driver.vehicle]]);
        return false
    }
    var dispatchtime = new Date();
    var id = driver.addRide(customer,  lat, long, customer.destination, uniqueId,customer.additionalInfo,customer.fullname,customer.phonenumber,dispatchtime);

    //Send to driver that customer looks for a ride
    driver.client.sendEvent(resEvents.requestRide,[id, lat, long, customer.destination, customer.additionalInfo]);
    return id;
};

self.sendRadioRideRequest = function (driver, lat, long, vehicle, customer, uniqueId) {
    if (driver == null) {
        console.log("DRIVER NULL");
        return false;
    }
    
    var id = driver.addRide(customer, lat, long, customer.destination, uniqueId);

    //Send to driver that customer looks for a ride
    driver.client.sendEvent(resEvents.requestRide, [id, lat, long, customer.destination, customer.additionalInfo]);
    return id;
};

self.sendAlreadyExists = function(client)
{
    client.sendEvent(resEvents.driverAlreadyExists);
};
self.sendLoginSuccessful = function(client, id)
{
    client.sendEvent(resEvents.loginSuccessful);
};
self.sendLoginFailed = function(client)
{
    client.sendEvent(resEvents.loginFailed);
};
self.sendNoSuchUser = function(client)
{
    client.sendEvent(resEvents.noSuchUser);
};
self.sendRequestCancelled = function(rideId,client)
{
    client.sendEvent(resEvents.requestCancelled,[rideId]);
};
self.sendActualRideCancelled = function(client)
{
    client.sendEvent(resEvents.actualRideCancelled);
};

self.sendReconnection = function(driver)
{
    //TODO reconnection
    driver.client.sendEvent(resEvents.reconnection, []);
};
self.sendMessage = function(driver, message)
{
    driver.client.sendEvent(resEvents.sendMessage, message);
};

self.sendHeatmap = function(client, heatmap)
{
    for(var latLng in heatmap)
    {
        client.sendEvent(resEvents.heatmapData, latLng + ":" + heatmap[latLng]);
    }
    client.sendEvent(resEvents.endHeatmap);
};
/**
 *
 * @param driver
 * @param searched
 * @returns Boolean
 */
function matchVehicles(driver, searched)
{

    var w_driver = config.VEHICLE_WEIGTH[ driver.toLowerCase().trim() ];
    var w_search = config.VEHICLE_WEIGTH[ searched.toLowerCase().trim() ]
    if(w_search == config.VEHICLE_WEIGTH.sedan) return true;

    return w_driver >= w_search
}