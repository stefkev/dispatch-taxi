/**
 * Created by Ankit Vaghela on 22.12.13.
 */
var config = require('./../../config');
var memored = require("memored");
var database = require("./../database/database_handler");
var Driver = require("./driver").Driver;
var driverSender = require("./driver_sender");
var clientResEvents = require("./../client_lib/events").clientReponseEvents;
var clientList = require("./../../client_handler/client_list");
var geolib = require("geolib");
//var bufferCoder = require("./../buffer_coder");
var db = require("./../../logic/database/database_handler");
var deasync = require("deasync")

var driverList = {};
module.exports = driverList;

// var list = {};
memored.store('driverList', []);

var CAB_COUNT = config.CAB_COUNT;
var MAX_DISTANCE = config.MAX_SQUARE_DISTANCE;
var MULTIPLIER = config.MULTIPLIER;

var http = require('http');
http.createServer(function(req, res) {});

var listall = function(){
    var masterlist = [];
    memored.read('driverList', function(err, list) {
        list.forEach(function(data){
            masterlist.push(data);
        })
    });
    return masterlist;
}

driverList.list = listall();


/**
 *
 * @param userId
 * @returns {Driver}
 */
driverList.getDriver = function(userId) {
    memored.read('driverList', function(err, list) {
        return list[userId];
    })
};
driverList.addDriver = function(userId, vehicle, fullname, client, licensePlate, phonenumber) {
    var driver = new Driver(userId, vehicle, fullname, client, licensePlate, phonenumber, Math.floor(Date.now() / 1000));
    memored.read('driverList', function(err, list) {
        list[userId] = driver;
        memored.store('driverList', list)
    })
    update({ event: events.add, driver: driver })
};
driverList.getList = function() {
    memored.read('driverList', function(err, list) {
        return list;
    })

};
driverList.removeDriver = function(userId) {
    memored.read('driverList', function(err, list) {
        var driver = list[userId];
        if (driver) driver.dispose();
        list[userId] = null;
        delete list[userId];
        memored.store('driverList', list)
        update({ event: events.remove, driver: driver })
    })


};
driverList.disconnectDriver = function(userId) {
    memored.read('driverList', function(err, list) {
        var driver = list[userId];
        update({ event: events.disconnect, driver: driver });
    })

};
driverList.changeDriverDuty = function(driver, onduty) {
    if (client.driver) client.driver.onDuty = (data == "1");
    update({ event: events.duty, driver: driver, onDuty: onduty })
};
driverList.updatePosition = function(userId, latitude, longitude) {
    memored.read('driverList', function(err, list) {
        latitude = latitude / MULTIPLIER;
        longitude = longitude / MULTIPLIER;

        if (config.POSITION_LOG_ENABLED) {
            database.logPosition(userId, latitude, longitude)
        }

        if (!list[userId]) {
            console.warn("user with userid: " + userId + " doesn't exist");

            var options = {
                host: '104.131.55.190',
                port: 80,
                path: '/gcm_server_php/send_message1.php?Id=' + userId,
                method: 'GET'
            };

            http.request(options, function(res) {
                res.setEncoding('utf8');
                res.on('data', function(chunk) {
                    console.log('BODY: ' + chunk);
                });
            }).end();

            return;
        }
        var driver = list[userId];
        list[userId].latitude = parseFloat(latitude);
        list[userId].longitude = parseFloat(longitude);
        memored.store('driverList', list)
        if (process.debug) console.log("User (" + userId + ") changed position to :" + latitude + "x" + longitude);

        if (driver.ride) {
            driver.ride.customer.client.sendEvent(clientResEvents.driverMoved, [latitude, longitude]);
        }
        update({ event: events.move, driver: driver })
    })



};
driverList.getFreeDriversCount = function(cb) {
    memored.read('driverList', function(err, list) {
        var i = 0;
        var driver;
        for (var driverId in list) {
            driver = list[driverId];
            if (driver && driver.online && driver.onDuty && !driver.ride) i++
        }
        cb(null, i)
    })

};
driverList.getDriversCount = function(cb) {
    memored.read('driverList', function(err, list) {
        var i = 0;
        var driver;
        for (var driverId in list) {
            driver = list[driverId];
            if (driver && driver.online && driver.onDuty) i++
        }
        cb(null, i)
    })
};
driverList.lookForClosestCab = function(customer, vehicle) {
    memored.read('driverList', function(err, list) {
        var latitude = customer.latitude;
        var longitude = customer.longitude;
        var closest = null,
            excludes = [],
            noMore = false;

        if (config.ALL_CABS) {
            for (var a in list) {
                if (list.hasOwnProperty(a) || list[a].onDuty) {
                    closest = list[a];
                    customer.client.sendEvent(clientResEvents.driverFound, [closest.id, closest.latitude, closest.longitude, closest.vehicle, closest.rpm]);
                }
            }
        } else {
            for (var i = 0; i < CAB_COUNT; i++) {
                closest = noMore ? null : driverList.findClosestDriver(latitude, longitude, excludes, true);
                if (closest) {
                    customer.client.sendEvent(clientResEvents.driverFound, [closest.id, closest.latitude, closest.longitude, closest.vehicle, closest.rpm]);
                    excludes.push(closest.id);
                } else {
                    noMore = true;
                }
            }
            customer.client.sendEvent(clientResEvents.noDriverFound);
        }
    })

};

function toRad(val) {
    return val * (Math.PI / 180);
}

function haversine(lat1, lon1, lat2, lon2) {
    lat1 = Number(lat1);
    lon1 = Number(lon1);
    lat2 = Number(lat2);
    lon2 = Number(lon2);

    var R = 6371; // km
    var dLat = toRad((lat2 - lat1));
    var dLon = toRad((lon2 - lon1));
    lat1 = toRad(lat1);
    lat2 = toRad(lat2);

    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    return Math.round(R * c * 1000) / 1000;
}

function pythagorianSquare(lat1, lon1, lat2, lon2) {
    lat1 = Number(lat1);
    lon1 = Number(lon1);
    lat2 = Number(lat2);
    lon2 = Number(lon2);

    return Math.pow(lat1 - lat2, 2) + Math.pow(lon1 - lon2, 2)
}

function pythagorian(lat1, lon1, lat2, lon2) {
    return Math.sqrt(pythagorianSquare(lat1, lon1, lat2, lon2))
}

driverList.receiveMessage = function(driver, msg) {
    update({ event: events.message, driver: driver, msg: msg.toString() })
};

driverList.findClosestDriver = function(lat, lng, excludes, squareInsteadOfCircle) {
    memored.read('driverList', function(err, list) {
        var driver = null,
            distance = -1,
            closestDistance = config.MAX_SQUARE_DISTANCE,
            closestDriver = null;

        for (var driverId in list) {
            if (list.hasOwnProperty(driverId) && list[driverId].onDuty && list[driverId].online) {
                driver = list[driverId];
                distance = squareInsteadOfCircle ?
                    pythagorianSquare(driver.latitude, driver.longitude, lat, lng) : haversine(driver.latitude, driver.longitude, lat, lng);

                if (distance < closestDistance && excludes.indexOf(driver.id) == -1 && driver.online && !driver.ride && !process.isAboutToQuit) {
                    closestDistance = distance;
                    closestDriver = driver;

                }
            }
        }
        return closestDriver;
    })

};

driverList.findClosestDriver1 = function(lat, lng, excludes, squareInsteadOfCircle) {

    memored.read('driverList', function(err, list) {
        var driver = null,
            distance = -1,
            closestDistance = config.MAX_SQUARE_DISTANCE,
            closestDriver = null;

        db.getZoneData(function(data) {
            zone = data;

            for (var driverId in list) {
                if (list.hasOwnProperty(driverId) && list[driverId].onDuty && list[driverId].online) {
                    driver = list[driverId];

                    //if(distance < closestDistance && excludes.indexOf(driver.id) == -1
                    //    && driver.online && !driver.ride && !process.isAboutToQuit)

                    // zone 

                    for (var i = 0; i < driverList.list.length; i++) {
                        var isTrue = true;


                        for (var j = 0; j < zone.length; j++) {

                            var objArray = [];

                            var stringcords = zone[j].latitude;
                            var subcoords = stringcords.split(',');

                            var stringcords1 = zone[j].longitude;
                            var subcoords1 = stringcords1.split(',');

                            for (var jj = 0; jj < subcoords.length; jj++) {
                                var obj = {};
                                obj.lat = parseFloat(subcoords[jj]);
                                obj.lng = parseFloat(subcoords1[jj]);
                                objArray.push(obj)
                            }
                            zone[j].polydata = objArray;

                            if (geolib.isPointInside({ latitude: parseFloat(driver.latitude), longitude: parseFloat(driver.longitude) }, objArray)) {

                                closestDriver = driver;
                                console.log("Client app dispatch: " + closestDriver);
                                return closestDriver;

                            }

                            // not zone

                            //distance = squareInsteadOfCircle ?
                            //    pythagorianSquare(driver.latitude, driver.longitude, lat,lng)
                            //    : haversine(driver.latitude, driver.longitude, lat, lng);

                            //if(distance < closestDistance && excludes.indexOf(driver.id) == -1
                            //    && driver.online && !driver.ride && !process.isAboutToQuit)
                            //{
                            //    closestDistance = distance;
                            //    closestDriver = driver;

                            //}
                        }
                    }
                }
            }

        });
    })
};

driverList.smsSent = function(driverId) {
    update({ event: events.sms, id: driverId })
};

function clone(a) {
    var b = {};
    for (var prop in a) {
        if (a.hasOwnProperty(prop)) b[prop] = a[prop];
    }
    return b;
}

//Observer pattern
// var observers = [];
memored.store('driverObserver', [])
module.exports.events = { add: 0, remove: 1, move: 2, reject: 4, disconnect: 5, message: 6, success: 7, sms: 8, duty: 9 };
var events = module.exports.events;
module.exports.subscribe = function(obj) {
    if (typeof(obj) != "function") throw new Error("Not a notify method");
    memored.read('driverObserver', function(err, observers) {
        observers.push(obj)
        memored.store('driverObserver', observers)
    })

};
var update = function(data) {
    memored.read('driverObserver', function(err, observers) {
        observers.forEach(function(x) {
            x(data);
        });
    })

};
module.exports.update = update;
//END OBSERVER
