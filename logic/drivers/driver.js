/**
 * Created by Iraasta on 22.12.13.
 */
var Ride        = require("./../rides/ride");
var database    = require("./../database/database_handler");
var config      = require("./../../config");
//var clientHandler = require("./../clients/client_handler");

/**
 *
 * @param id
 * @param vehicle
 * @param fullname
 * @param client
 * @constructor
 * @param licensePlate
 * @param phonenumber
 */
var Driver  = function(id, vehicle, fullname, client, licensePlate, phonenumber, logintime)
{
    this.phonenumber = phonenumber
    this.client = client;
    client.driver = this;
    this.online = true;
    this.onDuty = true;
    this.latitude = 0;
    this.longitude = 0;
    this.vehicle = vehicle;
    this.id = id;
    this.fullname = fullname;
    this.pendingRides = {};
    this.ride = null;
    this.rpm = "0";
    this.licensePlate = licensePlate;
	this.logintime = logintime;
	
    if(false) {
        this.positionLoggerTimeoutId = setInterval(function () {
            database.logPosition(client.driver.id, client.driver.latitude, client.driver.longitude)
        }, config.POSITION_LOG_TIMOUT);
    }
};

/**
 *
 * @param customer
 * @returns {number}
 * @param lat
 * @param lng
 * @param dest
 */
Driver.prototype.addRide = function(customer, lat, lng, dest, uniqueId,additionalInfo,fullname,phonenumber,dispatchtime)
{

    //GET NEXT EMPTY RIDE
    var from = [lat, lng];
    var to = dest.split(",");
    var i = 0;
    var additionalInfo = additionalInfo;
    var fullname = fullname;
    var phonenumber = phonenumber;
    var dispatchtime = dispatchtime;
    while(this.pendingRides[++i]){}
    this.pendingRides[i] = new Ride.Ride(this,customer, from, to, i, uniqueId,additionalInfo,fullname,phonenumber,dispatchtime);
    if(process.debug) console.log("ADDED RIDE");
    return i;
};
/**
 *
 */
Driver.prototype.rejectAllRides = function()
{
    for(var rideId in this.pendingRides)
    {
        if (process.debug) console.log("Rejecting one of the pending rides");
        if(this.pendingRides.hasOwnProperty(rideId))this.rejectRide(rideId);
    }
};

/**
 *
 * @param rideId
 */
Driver.prototype.rejectRide = function(rideId)
{

    if(process.debug) console.log("Ride rejected");
    require("./../clients/customer_handler").rejectTheRide(this.pendingRides[rideId].customer.client);
   
    this.logintime = Math.floor(Date.now() / 1000);
	
    this.pendingRides[rideId].dispose()
    delete this.pendingRides[rideId];
};

/**
 *
 * @param rideId
 */
Driver.prototype.acceptRide = function(rideId)
{
    console.log("acceptRide id:"+this.id);
	//console.log("This id:"+this.fullname);
    require("./../clients/customer_handler").acceptTheRide(this.pendingRides[rideId].customer.client,this.id);
    this.ride = this.pendingRides[rideId];
    this.ride.setStage(Ride.stages.waitingForPickup);
    delete  this.pendingRides[rideId];
    this.rejectAllRides();
	
	this.logintime = Math.floor(Date.now() / 1000);

    database.decrementFreeDriverCount();
};

/** 
 *
 * @param client
 * @returns {*}
 */
Driver.prototype.getRideIdByClient = function(client)
{
    for(var rideId in this.pendingRides)
    {
        if(this.pendingRides.hasOwnProperty(rideId))
            if(this.pendingRides[rideId].customer.client === client)return rideId;
    }
    return null;
};

/**
 *
 * @param rideId
 */
Driver.prototype.cancelRequest = function(rideId)
{
    if(process.debug) console.trace("CANCELLING RIDE REQUEST GOT YYA")
    this.pendingRides[rideId].rejectRideRequest();
    delete this.pendingRides[rideId];
};

/**
 *
 */
Driver.prototype.cancelActualRide = function()
{
    if(this.ride) this.ride.rejectRideRequest();
    this.ride = null;
};

/**
 *
 */
Driver.prototype.clientPickUp = function()
{
    this.ride.setStage(Ride.stages.driving);
    require("./../clients/customer_handler").pickup(this.ride.customer.client);
};

/**
 *
 */
Driver.prototype.clientDropOff = function()
{
     require("./../clients/customer_handler").dropoff(this.ride.customer.client);
    this.logintime = Math.floor(Date.now() / 1000);
    //remove all that shit to GC
    this.ride.dispose();
    this.ride.setStage(Ride.stages.finished);
};

/**
 *
 */
Driver.prototype.dispose = function()
{
    if(false){
        clearTimeout(this.positionLoggerTimeoutId);
    }
    database.decrementDriverCount();
    database.decrementFreeDriverCount();
};

module.exports.Driver = Driver;