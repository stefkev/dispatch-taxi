/**
 * Created by Iraasta on 22.12.13.
 */
var db              = require("./../database/database_handler");
var driverSender    = require("./driver_sender");
var driverList      = require("./drivers_list");
var security        = require("./../security/password_security");
var clientHandler   = require("./../clients/customer_handler");
var reconnectHandler= require("./../reconnect_handler");

var driverHandler = {};
module.exports = driverHandler;

driverHandler.logoutCab = function(client)
{
driverList.removeDriver(client);
    client.disconnect();
};
driverHandler.loginCab = function(client, login, password, isDisconnect, cb)
{
    if(!security.checkIfValid(login) || !security.checkIfValid(password))
    {
        driverSender.sendLoginFailed(client);
        return;
    }
    //TODO HASHING
    db.loginCab(login, password, function(loginExists, passwordMatches, result){
        if(!loginExists)
        {
            driverSender.sendNoSuchUser(client); 
        }
        else{
            if(passwordMatches) {
                process.nextTick(cb);
                driverSender.sendLoginSuccessful(client, result.id);

                //TODO RECONNECTION HANDLER IN DRIVER
                if (reconnectHandler.checkRecentDisconnect(client, result.id))
                {
                    client.driver.vehicle = result.vehicle

                    return;
                }
				console.log("DRIVER "+ result.firstname + "Disconnect : "+ isDisconnect);
                driverList.addDriver(result.id, result.vehicle, result.firstname + " " + result.lastname, client, result.driverslicense, result.phonenumber);
				
            }
            else
            {
                driverSender.sendLoginFailed(client);
            }
        }
    });
};

/**
 *
 * @param client
 * @param latitude
 * @param longitude
 */
driverHandler.updatePosition = function(latitude, longitude, client)
{
		if(client.driver == undefined){
			return;
			}
    if(!client.driver)
    {
        console.log("not loggd in but moving"+client.driver.id);
        return;
    }
	
    var userid = client.driver.id;
    driverList.updatePosition(userid,latitude,longitude);
};
driverHandler.acceptRide = function(rideId,client)
{
    if(client.driver && client.driver.pendingRides[rideId])
    {
        client.driver.acceptRide(rideId);
        driverList.update({event: driverList.events.success, id: client.driver.id, uniqueId: client.driver.ride.uniqueId})
    }
    else
    {
        console.log("Something went wrong");
        console.log(client);
        clientHandler.sthWentWrong(client);
    }

};
driverHandler.rejectRide = function(rideId,client)
{
    if(client.driver && client.driver.pendingRides[rideId])
    {
        driverList.update({event: driverList.events.reject, id: client.driver.id, uniqueId: client.driver.pendingRides[rideId].uniqueId})
        client.driver.rejectRide(rideId);
    }
    else
    {
        clientHandler.sthWentWrong(client);
    }
};
driverHandler.clientPickUp = function(client)
{
    if(client.driver && client.driver.ride)
    {
        client.driver.clientPickUp();
    }
    else
    {
        clientHandler.sthWentWrong(client);
    }
};
driverHandler.clientDropOff = function(client)
{
    if(client.driver && client.driver.ride)
    {
        client.driver.clientDropOff();
    }
    else
    {
        clientHandler.sthWentWrong(client);
    }
};

driverHandler.reconnectDriver = function(driver)
{
    driverSender.sendReconnection(driver);
};
driverHandler.receiveMessage = function(msg, client)
{
    if(!isLoggedIn(client)) return;
    var driver = driverList.getDriver(client.driver.id);
    driverList.receiveMessage(driver, msg);
};

driverHandler.getHeatmap = function(client)
{
    db.getHeat(function(err, heat)
    {
        driverSender.sendHeatmap(client, heat);
    })
};
function isLoggedIn(client)
{
    return !!client.driver
}