/**
 * Created by iraasta on 19/04/14.
 */
var driver_list     = require("./../drivers/drivers_list");
var driver_sender   = require("./../drivers/driver_sender");
var config          = require("./../../config");

exports.makeRequest = function(driverId, pos, dest, client)
{
    var customer = {
        destination: dest,
        additionalInfo: "Manual dispatch",
        isManual: true,
        client: client
    };
    var driver = driver_list.getDriver(driverId);
    pos = pos.split(",");
    lat = pos[0];
    long = pos[1];
    driver_sender.sendRideRequest(driver,lat,long, customer);

    setTimeout(function(){
        cancelRequest(driverId, null , false);
    },config.AUTO_CANCEL_DELAY)
};
var cancelRequest = function(driverId, client, remote)
{
    var driver = driver_list.getDriver(driverId);
    var rideId =  driver.getRideIdByClient(client);
    if(remote && !rideId)
    {
        driver.cancelActualRide();
        driverSender.sendActualRideCancelled(driver.client);
    }
    else if(rideId )
    {
        driver.cancelRequest(rideId);
        driver_sender.sendRequestCancelled(driverId, driver.client);
        if(process.debug)console.log("Ride of " + driverId + " cancelled");
    }
};