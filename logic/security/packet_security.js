/**
 * Created by Iraasta on 22.12.13.
 */

var packetSecurity = {};
module.exports = packetSecurity;
/**
 *
 * @param {number} length
 * @param {Array} tab
 */
var SECURITY_ERROR = "SECURITY ERROR::";
/**
 *
 * @param length
 * @param tab
 * @returns {Boolean}
 */
packetSecurity.checkPacketLength = function(length,tab)
{
    if(tab.length != length || (tab.length == 1 && tab[0] == ""))
    {
        console.log (SECURITY_ERROR + "Packet length equal " + tab.length + " (should be " + length + ")");
        return false;
    }
    return true;
};
/**
 *
 * @param client
 * @returns {boolean}
 */
packetSecurity.checkIfLoggedIn = function(client)
{
    return (client.driver || client.customer)
};