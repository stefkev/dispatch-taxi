/**
 * Created by Chlopaki on 29.12.13.
 */
var passwordSecurity = {};
module.exports = passwordSecurity;

var passwordRegExp = /[a-zA-Z]|@|\.|\d/;
/**
 *
 * @param str
 * @returns {boolean}
 */
passwordSecurity.checkIfValid = function(str)
{   
return passwordRegExp.test(str);
};
