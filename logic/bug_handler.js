/**
 * Created by Iraasta on 27.02.14.
 */
var bug_handler = {};
module.exports = bug_handler;

//========IMPORTS============
var fs = require("fs");
var path = require("path");
//==========END==============
module.exports.report  = function(data)
{
    console.log("======== ERROR REPORT REPORT =======");
    console.log(data);
    console.log("======== ERROR REPORT END ==========");
    fs.appendFile(path.join(__dirname, ".." , "bug_reports.txt"),data + "\n");
};