/**
 * Created by root on 25/04/14.
 */
var memored = require("memored")
exports.events = { add: 0, remove: 1, updateState: 2, moved: 3, reject: 4 };


// var list = [];
memored.store('ride', []);
memored.store('rideObserver', []);
exports.addRide = function(ride) {
    memored.read('ride', function(err, list) {
        for (var i = 0; i < list.length; i++) {
            if (list[i] == null) {
                list[i] = ride;
                update({ event: exports.events.add, ride: ride });
                return i;
            }
        }
        update({ event: exports.events.add, ride: ride });
        list.push(ride) - 1;
        memored.store('ride', list)
        memored.read('ride', function(err, newride) {
            return newride
        })
    })


};
exports.removeRide = function(ride) {
    //I Don't update because ride changes stage anyway
    memored.read('ride', function(err, list) {
        list[list.indexOf(ride)] = null;
        ride.index = -1;
        momored.store('ride', list)
    })

};
exports.getLength = function() {
    var cnt = 0;
    memored.read('ride', function(err, list) {
        for (var i = 0; i < list.length; i++) {
            if (list[i] != null) cnt++
        }
        return cnt;
    })


};
exports.getList = function() {
    var masterlist = []
    memored.read('ride', function(err, list) {
        list.forEach(function(data){
            masterlist.push(data)
        })
    })
    return masterlist
};
exports.emergencyCancelRide = function(id) {
    memored.read('ride', function(err, list) {
        if (!list[id]) return;
        list[id].emergencyCancel();
        list[id] = null;
        delete list[id]
        memored.store('ride', list)
    })

};

// var observers = [];
exports.subscribe = function(obj) {
    // var observers = []
    if (typeof(obj) != "function") throw new Error("Not a notify method");
    
    memored.read('rideObserver', function(err, observers){
        observers.push(obj);
        memored.store('rideObserver', observers)
    })
    
};
exports.unsubscribe = function(obj) {
    memored.read('rideList', function(err, observers) {
        delete observers[observers.indexOf(obj)]
        memored.store('rideList', observers)
    })

}

function update(data) {
    memored.read('rideList', function(err, observers) {
        for (var i = 0; i < observers.length; i++) {
            observers[i](data);
            memored.store('rideList', observers)
        }
    })

}
exports.update = update;
