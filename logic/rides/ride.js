/**
 * Created by Iraasta on 22.12.13.
 */
var config = require("./../../config");
var ride_list = require("./ride_list");
var database = require("./../database/database_handler");
var events      = require("./../client_lib/events");

var ride = {};
module.exports  = ride;

/**
 *
 * @param driver
 * @param customer
 * @param from
 * @param to
 * @constructor
 * @param id
 * @param uniqueId
 */
ride.Ride = function(driver,customer, from, to, id, uniqueId,additionalInfo,fullname,phonenumber,dispatchtime)
{
    this.uniqueId = uniqueId;
    this.id = id;
    this.from = from;
    this.to = to;
    this.driver = driver;
    this.customer = customer;
    customer.ride = this;
    this.stage = ride.stages.pending;
    this.additionalInfo = additionalInfo;
    this.fullname = fullname;
    this.phonenumber = phonenumber;
    this.dispatchtime = dispatchtime;
    this.index = ride_list.addRide(this);
};

/**
 *
 * @param stage
 */
ride.Ride.prototype.setStage = function(stage)
{
    this.stage = stage;
    ride_list.update({event: ride_list.events.updateState, ride : this});
    database.setRide(this.driver.id, this);
    if(stage == ride.stages.finished)
    {
        database.removeRide(this.driver.id);
        this.driver.ride = null;
        this.customer.ride = null;
    }
};

ride.Ride.prototype.getState = function(){
    return [
        this.stage,
        this.id,
        this.from[0],
        this.from[1],
        this.to[0],
        this.to[1]
    ].join(" ");
};

ride.Ride.prototype.rejectRideRequest = function()
{
    ride_list.removeRide(this);
    //ride_list.update({event: ride_list.events.reject, ride: this});
};

ride.Ride.prototype.dispose = function()
{
    ride_list.removeRide(this);
};
ride.Ride.prototype.emergencyCancel = function()
{
    this.driver.client.sendEvent(events.responseEvents.emergencyCancel);
    this.driver.cancelActualRide();

    this.customer.client.sendEvent(events.clientReponseEvents.emergencyCancel);
    this.customer.ride = null;
    this.setStage(ride.stages.finished);
};
ride.stages = config.RIDE_STAGES;