db            = require "./../database/database_handler"
cronclock     = require "cronclock"
config        = require "./../../config"
driverList    = require "./../drivers/drivers_list";
rideList      = require "./../rides/ride_list"
driverSender  = require "./../drivers/driver_sender"
reservations  = {}
reservedRidesCallbacks = {}


rideList.subscribe((data)->
  if data.ride? and reservedRidesCallbacks[data.ride.id]?
    if data.event == rideList.events.reject
      reservedRidesCallbacks[data.ride.id](false)
    else if data.event == rideList.events.updateState && data.ride.stage == config.RIDE_STAGES.waitingForPickup
      reservedRidesCallbacks[data.ride.id](true)
)

findNext = (id, rId, fromLatLng, toLatLng, excludes, stackProtector)->
  if stackProtector <= 0
    console.log("!!!NO DRIVER FOR RESERVATION FOUND!!!")
    exports.cancelReservation(id,rId,(success)-> if not success then console.log("something wrong happened"))
    return
  closest = driverList.findClosestDriver(fromLatLng[0], fromLatLng[1], excludes, false)
  if not closest?
    excludes = []
    findNext id, rId, fromLatLng, toLatLng, excludes, stackProtector - 1
  else
    console.log("Driver found in " + (config.STACK_MAX_RECURSION - stackProtector) + " steps");
    excludes.push(closest.id)
    rideId = driverSender.sendRideRequest(closest, fromLatLng[0], fromLatLng[1], new FakeCustomer(toLatLng, fromLatLng))

  reservedRidesCallbacks[rideId] = (found) ->
    if not found then findNext id, rId, fromLatLng, toLatLng, excludes, stackProtector - 1
    else delete reservedRidesCallbacks[rideId]

setReservation = (customerId, reservationId, dateInMillis, fromLatLng, toLatLng) ->
  if not reservations[customerId]? then reservations[customerId] = {};

  reservations[customerId][reservationId] = cronclock.add(new Date(dateInMillis), ->
    findNext(customerId, reservationId, fromLatLng, toLatLng, [], config.STACK_MAX_RECURSION)
  )
  if dateInMillis < Date.now()
    findNext(customerId, reservationId, fromLatLng, toLatLng, [], config.STACK_MAX_RECURSION)


exports.makeReservation = (customerId, date, minutesBefore, fromLatLng, toLatLng, vehicleType, cb) ->
  db.getReservations(customerId, (result)->
    tab = null
    if !result or Object.keys(result).length == 0
      tab = {}
      biggestIndex = -1
    else
      tab = result
      biggestIndex = Number((key for key, _ of tab).reduce (a,b) -> if Number(a) > Number(b) then Number(a) else Number(b))

    tab[biggestIndex+1] =
      index : biggestIndex+1,
      customerId: customerId,
      date:date.getTime(),
      minutesBefore:minutesBefore,
      fromLatLng:fromLatLng,
      toLatLng:toLatLng,
      vehicleType:vehicleType

    console.log(tab);

    db.setReservations(customerId, tab);
    setReservation(customerId, biggestIndex+1, date.getTime() - minutesBefore*60000, fromLatLng, toLatLng)
    cb(biggestIndex+1)
  )

exports.cancelReservation = (customerId, reservationId, cb) ->
  db.getReservations(customerId, (result)->
    success = true;
    if result?
      success = success && delete result[reservationId]
      success = success && delete reservations[customerId][reservationId]
      db.setReservations(customerId, result);
      cb(success)
    
    else
      cb(false)
  )

exports.startAllReservations = () ->
  db.getAllReservations((result)->
    for key, value of result
      value = JSON.parse(value);
      for key2, value2 of value
        setReservation(key, key2, value2.date, value2.fromLatLng, value2.toLatLng)
    console.log("RESERVATIONS SUCCESSFULLY LOADED")
  )

exports.getAllReservationsOf = (customerId, cb) ->
  db.getReservations(customerId, (result)->
    if result
      cb result
    else cb {}
  )

exports.startAllReservations();


class FakeCustomer
  constructor: (destination, position) ->
    @client = {sendEvent: ->};
    @client.customer = this;
    @fullname = "Automatic reservation";

    @latitude = position[0];
    @longitude = position[1];
    @destination = destination.join(',')
    @additionalInfo = "Automatic reservation"
    @ride = undefined;
    @id = -1;
    @jumps = 0;

  update: ->
  disconnected: ->
  rideFound: ->
