var FakeCustomer, config, cronclock, db, driverList, driverSender, findNext, reservations, reservedRidesCallbacks, rideList, setReservation;

db = require("./../database/database_handler");

cronclock = require("cronclock");

config = require("./../../config");

driverList = require("./../drivers/drivers_list");

rideList = require("./../rides/ride_list");

driverSender = require("./../drivers/driver_sender");

reservations = {};

reservedRidesCallbacks = {};

rideList.subscribe(function(data) {
  if ((data.ride != null) && (reservedRidesCallbacks[data.ride.id] != null)) {
    if (data.event === rideList.events.reject) {
      return reservedRidesCallbacks[data.ride.id](false);
    } else if (data.event === rideList.events.updateState && data.ride.stage === config.RIDE_STAGES.waitingForPickup) {
      return reservedRidesCallbacks[data.ride.id](true);
    }
  }
});

findNext = function(id, rId, fromLatLng, toLatLng, excludes, stackProtector) {
  var closest, rideId;
  if (stackProtector <= 0) {
    console.log("!!!NO DRIVER FOR RESERVATION FOUND!!!");
    exports.cancelReservation(id, rId, function(success) {
      if (!success) {
        return console.log("something wrong happened");
      }
    });
    return;
  }
  closest = driverList.findClosestDriver(fromLatLng[0], fromLatLng[1], excludes, false);
  if (closest == null) {
    excludes = [];
    findNext(id, rId, fromLatLng, toLatLng, excludes, stackProtector - 1);
  } else {
    console.log("Driver found in " + (config.STACK_MAX_RECURSION - stackProtector) + " steps");
    excludes.push(closest.id);
    rideId = driverSender.sendRideRequest(closest, fromLatLng[0], fromLatLng[1], new FakeCustomer(toLatLng, fromLatLng));
  }
  return reservedRidesCallbacks[rideId] = function(found) {
    if (!found) {
      return findNext(id, rId, fromLatLng, toLatLng, excludes, stackProtector - 1);
    } else {
      return delete reservedRidesCallbacks[rideId];
    }
  };
};

setReservation = function(customerId, reservationId, dateInMillis, fromLatLng, toLatLng) {
  if (reservations[customerId] == null) {
    reservations[customerId] = {};
  }
  reservations[customerId][reservationId] = cronclock.add(new Date(dateInMillis), function() {
    return findNext(customerId, reservationId, fromLatLng, toLatLng, [], config.STACK_MAX_RECURSION);
  });
  if (dateInMillis < Date.now()) {
    return findNext(customerId, reservationId, fromLatLng, toLatLng, [], config.STACK_MAX_RECURSION);
  }
};

exports.makeReservation = function(customerId, date, minutesBefore, fromLatLng, toLatLng, vehicleType, cb) {
  return db.getReservations(customerId, function(result) {
    var _, biggestIndex, key, tab;
    tab = null;
    if (!result || Object.keys(result).length === 0) {
      tab = {};
      biggestIndex = -1;
    } else {
      tab = result;
      biggestIndex = Number(((function() {
        var results;
        results = [];
        for (key in tab) {
          _ = tab[key];
          results.push(key);
        }
        return results;
      })()).reduce(function(a, b) {
        if (Number(a) > Number(b)) {
          return Number(a);
        } else {
          return Number(b);
        }
      }));
    }
    tab[biggestIndex + 1] = {
      index: biggestIndex + 1,
      customerId: customerId,
      date: date.getTime(),
      minutesBefore: minutesBefore,
      fromLatLng: fromLatLng,
      toLatLng: toLatLng,
      vehicleType: vehicleType
    };
    console.log(tab);
    db.setReservations(customerId, tab);
    setReservation(customerId, biggestIndex + 1, date.getTime() - minutesBefore * 60000, fromLatLng, toLatLng);
    return cb(biggestIndex + 1);
  });
};

exports.cancelReservation = function(customerId, reservationId, cb) {
  return db.getReservations(customerId, function(result) {
    var success;
    success = true;
    if (result != null) {
      success = success && delete result[reservationId];
      success = success && delete reservations[customerId][reservationId];
      db.setReservations(customerId, result);
      return cb(success);
    } else {
      return cb(false);
    }
  });
};

exports.startAllReservations = function() {
  return db.getAllReservations(function(result) {
    var key, key2, value, value2;
    for (key in result) {
      value = result[key];
      value = JSON.parse(value);
      for (key2 in value) {
        value2 = value[key2];
        setReservation(key, key2, value2.date, value2.fromLatLng, value2.toLatLng);
      }
    }
    return console.log("RESERVATIONS SUCCESSFULLY LOADED");
  });
};

exports.getAllReservationsOf = function(customerId, cb) {
  return db.getReservations(customerId, function(result) {
    if (result) {
      return cb(result);
    } else {
      return cb({});
    }
  });
};

exports.startAllReservations();

FakeCustomer = (function() {
  function FakeCustomer(destination, position) {
    this.client = {
      sendEvent: function() {}
    };
    this.client.customer = this;
    this.fullname = "Automatic reservation";
    this.latitude = position[0];
    this.longitude = position[1];
    this.destination = destination.join(',');
    this.additionalInfo = "Automatic reservation";
    this.ride = void 0;
    this.id = -1;
    this.jumps = 0;
  }

  FakeCustomer.prototype.update = function() {};

  FakeCustomer.prototype.disconnected = function() {};

  FakeCustomer.prototype.rideFound = function() {};

  return FakeCustomer;

})();

// ---
// generated by coffee-script 1.9.2