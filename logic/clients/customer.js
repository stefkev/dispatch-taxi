/**
 * Created by Iraasta on 22.12.13.
 */

var heat_handler = require("./../heatmap_handler");

/**
 *
 * @param client
 * @param customerId

 * @constructor
 * @param fullname
 */
module.exports.Customer = function(client,customerId, fullname, phonenum)
{
    this.client = client;
    client.customer = this;
    this.fullname = fullname;

    this.ride = undefined;
    this.id = customerId;
    this.jumps = 0;
    this.additionalInfo = "none";
    this.phonenumber = phonenum;
};
/**
 *
 * @param [fullname]
 * @param [latitude]
 * @param [longitude]
 * @param [destination]
 * @param additional
 */
module.exports.Customer.prototype.update = function(fullname, latitude, longitude, destination, additional)
{
   this.latitude = latitude || this.latitude;
    this.longitude = longitude || this.longitude;
    this.destination = destination || this.destination;
    this.additionalInfo = additional || this.additionalInfo;
    this.fullname = fullname || this.fullname;
};
module.exports.Customer.prototype.disconnected = function()
{
    heat_handler.requestFailed(this.latitude, this.longitude);
    this.jumps = 0;
};
module.exports.Customer.prototype.rideFound = function()
{
    heat_handler.requestSuccessful(this.latitude, this.longitude, this.jumps);
    this.jumps = 0;
};