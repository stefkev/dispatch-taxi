/**
 * Created by Iraasta on 22.12.13.
 */;
//
var driver_list     = require("./../drivers/drivers_list");
var events          = require("./../client_lib/events").clientReponseEvents;
var driverSender    = require("./../drivers/driver_sender");
var customer        = require("./customer");
var db              = require("./../database/database_handler");
var security        = require("./../security/password_security");
var clientList      = require("./customer_list");
var config          = require("./../../config");
var reservations    = require("./../reservations/reservation_handler.js");
var reconnectHandler = require("./../reconnect_handler");
//
var AUTO_CANCEL_DELAY = config.AUTO_CANCEL_DELAY;
var RATE_MAX    = config.RATE_MAX;

var clientHandler = {};
module.exports = clientHandler;


clientHandler.lookForCab = function(customer,vehicle)
{
    driver_list.lookForClosestCab(customer,vehicle);
};
clientHandler.sthWentWrong = function(client)
{
    client.sendEvent(events.somethingWentWrong);
};
clientHandler.rejectTheRide = function(client)
{
    client.sendEvent(events.rideRejected);
};
clientHandler.acceptTheRide = function(client,id)
{
    client.customer.rideFound();
    client.sendEvent(events.rideAccepted,[id]);
	console.log("acceptTheRide:" + id);
	return id;
};
/**
 *
 * @param driverId
 * @param lat
 * @param long
 * @param fullname
 * @param dest
 * @param vehicle
 * @param date
 * @param minutesBefore
 * @param client
 * @param additional
 * @param phoneNumber
 * @returns {Number} id
 * @param uniqueId
 */
clientHandler.requestRide = function(driverId,lat,long,fullname,dest,vehicle ,date, minutesBefore ,client, additional, phoneNumber, uniqueId)
{
    if(parseInt(driverId) == -1)
    {
        reservations.makeReservation(client.customer.id, date, minutesBefore, [lat,long],dest.split(','), vehicle, function(id)
        {
            client.sendEvent(events.reservationSuccess);
            client.sendEvent(events.reservationsData, [
                date.getTime(),
                minutesBefore,
                [lat,long].join(","),
                dest,
                vehicle,
                id
            ])

        });
        return null;
    }
    client.customer.update(fullname,lat,long,dest, additional);
    if(phoneNumber)client.customer.phonenumber = phoneNumber;
    client.customer.jumps++;

    //COMMANDER QUIT
    if(process.isAboutToQuit)
    {
        clientHandler.cancelRequest(driverId, client, false);
        return null;
    }

    var driver = driver_list.getDriver(driverId);
    if(!driver || !driver.onDuty)
    {
        // driver already doesnt exists
        clientHandler.cancelRequest(driverId,client, false);
        return null
    }
    var id = driverSender.sendRideRequest(driver,lat,long, vehicle,client.customer, uniqueId);
    var failed = process.isAboutToQuit || (id == false);
    if(failed)
    {
        clientHandler.cancelRequest(driverId,client, false);
        driver_list.update({event: driver_list.events.reject,
                            id : driverId, uniqueId: uniqueId});
    }
    setTimeout(function(){
        clientHandler.cancelRequest(driverId,client, false);
    },AUTO_CANCEL_DELAY)
    return id
};


clientHandler.requestRadioRide = function (driverId, lat, long, fullname, dest, vehicle, date, minutesBefore, client, additional, phoneNumber, uniqueId) {
    var driver = driver_list.getDriver(driverId);
    var id = driverSender.sendRadioRideRequest(driver, lat, long, vehicle, client.customer, uniqueId);
    return id;
};



/**
 *
 * @param driverId
 * @param client
 * @param {boolean} [remote]
 */
clientHandler.cancelRequest = function(driverId, client, remote)
{
    var driver = driver_list.getDriver(driverId);
    if(!driver) return;
    var rideId =  driver.getRideIdByClient(client);
    if(remote && !rideId)
    {
        driver.cancelActualRide();
        driverSender.sendActualRideCancelled(driver.client);
    }
    else if(rideId)
    {

        driver.cancelRequest(rideId);
        driverSender.sendRequestCancelled(driverId, driver.client);
    }
    client.sendEvent(events.rideRejected);
    driver_list.update({event: driver_list.events.reject, id:driverId})
};
clientHandler.login = function(login, password,client, cb)
{
    if(!security.checkIfValid(login) || !security.checkIfValid(password))
    {
        client.sendEvent(events.loginFailed);
        return;
    }
    //TODO SECURITY
    db.getPrices(function(err, prices){
        db.loginPassenger(login,password, function(loginExists, passwordMatches, result){
            if(!loginExists)
            {
                client.sendEvent(events.noSuchUser);
            }
            else{
                if(passwordMatches)
                {
                    client.sendEvent(events.loginSuccessful, prices.split("|"));

                    process.nextTick( cb)
                    //TODO reconnection disabled
                    if(reconnectHandler.checkRecentDisconnect(client,result.id))return;

                    clientList.addCustomer(new customer.Customer(client , result.id, result.fullname, result.phonenumber));

                    reservations.getAllReservationsOf(result.id, function(res)
                    {
                        var reservation;
                        for(var id in res)
                        {
                            reservation = res[id];
                            client.sendEvent(events.reservationsData,
                            [
                                reservation.date,
                                reservation.minutesBefore,
                                reservation.fromLatLng.join(","),
                                reservation.toLatLng.join(","),
                                reservation.vehicleType,
                                id
                            ])
                        }
                    })
                }
                else
                {
                    client.sendEvent(events.loginFailed);
                }
            }
        });
    })
};
clientHandler.pickup = function(client)
{
    client.sendEvent(events.pickupConfirmed);
};
clientHandler.dropoff = function(client)
{
    client.sendEvent(events.dropoffConfirmed);
};
clientHandler.rateDriver = function(driverId, rate, client)
{
    if(rate > RATE_MAX || rate < 0) return;
    //TODO finish
};


/**
 *
 * @param client
 * @param name
 * @param login
 * @param password
 * @param email
 * @param phonenumber
 */
clientHandler.registerClient = function(client, name, login, password, email, phonenumber)
{
    db.registerClient(name, login, password, email, phonenumber, function(err)
    {
        if(err) client.sendEvent(events.registrationFailed);
        else client.sendEvent(events.registrationSuccess);
    });
};