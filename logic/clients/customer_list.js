/**
 * Created by Iraasta on 10.01.14.
 */
var memored = require("memored")
memored.store('customer', []);
var customer_list = {};
module.exports = customer_list;

//========IMPORTS============

//==========END==============
// var list = {};

var listAll = function(){
    var masterList = []
    memored.read('customer', function(err, list){
        list.forEach(function(data){
            masterList.push(data)
        })
    })
    return masterList
}
customer_list.list = listAll();

customer_list.addCustomer = function(customer) {
    memored.read('customer', function(err, list) {
        list[customer.id] = customer;
        memored.store('customer', list)
    })

};
customer_list.removeCustomer = function(id) {
    memored.read('customer', function(err, list) {
        if (list[id]) {
            list[id].disconnected();
            delete list[id];
        }
        memored.store('customer', list)
    })
};
