/**
 * Created by root on 30.06.14.
 */
var readline = require('readline');
var db      = require("./../logic/database/database_handler");
var config  = require("./../config");

var commands =
{
    H : {fun: helpCom,          description: "Show all commands"},
    Q : {fun: quitCom,          description: "Blocks the system and gracefully closes when last ride stops"},
    A : {fun: abortQuitCom,      description: "Aborts gracefull quit"},
    LD: {fun: listDriversCom,   description: "Lists all drivers and info about them"},
    LR: {fun: listRidesCom,     description: "Lists all rides and their info"},
    GPL: {fun: positionLogCom,  description: "List al postions log"}
};

var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

console.log("COMMANDER STARTED");
question();

function question()
{
    rl.question(">> ", function(answer) {
        handleCommand(answer.toString().trim());
        question();
    });
}

function handleCommand(command)
{
    var cake = command.split(" ");
    command = cake.splice(0,1);

    if(commands[command])
    {
        commands[command].fun(cake);
    }
}

function helpCom()
{
    console.log("===================================================");
    for(var a in commands)
    {
        console.log("Command: "+ a + " -> " + commands[a].description);
    }
    console.log("===================================================");
}
function quitCom(){
    process.isAboutToQuit = true;
    var rides;
    var list = require("./../logic/rides/ride_list");

    var cnt = list.getLength();
    console.log("STARTING GRACEFUL QUIT");
    console.log(cnt + " RIDES LEFT");
    if(cnt == 0 ) quit();
    process.quitID = setInterval(function()
    {
        if(list.getLength() != cnt)
        {
            cnt = list.getLength();
            if(cnt == 0) quit();
            else console.log(cnt + " RIDES LEFT")
        }
        process.stdout.write(".");
    },1000)
}
function quit(){
    console.log("ALL RIDES FINISHED. QUITTING");
    process.exit(0);
}
function abortQuitCom(){
    if(process.quitID && process.isAboutToQuit) {
        clearInterval(process.quitID);
        process.isAboutToQuit = false;
        console.log("Abort successful");
    }
    else
    {
        console.log("System in not during quit");
    }
}
function listDriversCom()
{
    var list = require("./../logic/drivers/drivers_list").getList();
    var e = null;

    console.log("===================================================");
    for(var a in list)
    {
        e = list[a];
        if(e == null) continue;
        console.log(a + ". ID: "+ e.id + " LatLng: " + e.latitude + "," + e.longitude + " Online/OnDuty: " +[e.online, e.onDuty]  + " RIDE: " +
                (e.ride ? e.ride.customer.id : "null"));
    }
    console.log("===================================================")
}
function listRidesCom()
{
    var list = require("./../logic/rides/ride_list").getList();

    console.log("===================================================");
    for(var i = 0 ; i < list.length; i++)
    {
        var e = list[i];
        if(!e) console.log(i + ". NULL");
        else console.log(i + ". ID: " + e.index + " DRIVER: " + e.driver.id + " CUSTOMER: " + e.customer.id);
    }
    console.log("===================================================")
}

function positionLogCom(rest)
{
    if(rest.length != 3) console.log("wrong parameters");
    var id = rest[0];
    var first = rest[1];
    var second = rest[2];

    db.getPositionLog(id, first, second, function(err, result)
    {
        console.log(" ");
        for(var i = 0; i < result.length; i++)
        {
            var cake = result[i].split("&");
            var b = new Buffer(cake[1]);
            console.log(cake[0] + " => " + (b.readInt32BE(0)/config.MULTIPLIER).toString()
                + " , " + (b.readInt32BE(4)/config.MULTIPLIER).toString())
        }
    })
}
