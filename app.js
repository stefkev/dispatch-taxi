// Cluster

var cluster = require('cluster');
var os = require('os');
var numCPUs = require('os').cpus().length;
var net = require('net');
var config = require('./config');
var framecut = require('framecut');
var Client = require('./client_handler/client');
var events = require("./logic/client_lib/events");
var redis = require('socket.io-redis');


if (cluster.isMaster) {
    // we create a HTTP server, but we do not use listen
    // that way, we have a socket.io server that doesn't accept connections
    // 
    var debug = process.execArgv.indexOf('--debug') !== -1;
    cluster.setupMaster({
        execArgv: process.execArgv.filter(function(s) {
            return s !== '--debug'
        })
    });

    for (var i = 0; i < numCPUs; i++) {
        cluster.fork();
    }

    console.log('Master cluster setting up ' + numCPUs + ' workers...');

    cluster.on('online', function(worker) {
        console.log('Worker ' + worker.process.pid + ' is online');
    });

    cluster.on('exit', function(worker, code, signal) {
        console.log('worker ' + worker.process.pid + ' died');
    });

} else {
    initialize();
}


function initialize() {
    var program = require('./program.js');
    program.init();



    var PORT = config.TCP_PORT;
    framecut.initByLength(program.handleData, config.FRAME_LENGTH_LENGTH);



    var server = net.createServer(function(sock) {


        sock.setKeepAlive(true, 7000);
        var frame = new Buffer(0);

        var ip = sock.remoteAddress;
        var port = sock.remotePort;
        if (process.debug) console.log("Connected " + ip + ":" + port);
        var client = new Client.Client(ip, port, sock);
        program.handleConnect(client);

        sock.on('data', function(data) {
            framecut.handleFrame(data, client);
        });

        sock.on('close', function() {
            client.disconnect();
            program.handleDisconnect(client);
        });
        sock.on('error', function() {
            if (process.debug) console.log('Interrupted');
        });

    }).listen(PORT);

    console.log('TCP SERVER LISTENING ON ' + PORT);

    if (config.USES_SOCKETIO) {
        debugger;
        // debug(config.USES_SOCKETIO)
        var port = config.SOCKET_IO_PORT;
        var io = require('socket.io')(port, { log: false });
        io.adapter(redis({ host: 'localhost', port: 6379 }));
        var sockets = io;
        sockets.on('connection', function(sock) {
            console.log("got new sockio conn")
            var address = sock.handshake.address;
            var client = new Client.SioClient(address.address, address.port, sock, false);
            sock.on('disconnect', function() {
                console.log("Handle disconnect code run");
                program.handleDisconnect(client);
                client.disconnect();
                client = null;
            });
            //SMART wrap trick
            var evs = events.requestEvents;
            for (var ev in evs) {

                if (evs.hasOwnProperty(ev))
                    handleSockioData(client, sock, evs[ev])
            }
        });
        console.log("SOCKET.IO MAIN SERVER STARTED ON PORT " + port);

    }

    function handleSockioData(client, sock, ev) {
        sock.on(ev, function(data) {
            //console.log("GOT MESSAGE "+ ev + " " + data)
            try {
                program.handleData(data, client, ev)
            } catch (e) {
                console.log("Some errors occurred\n" + e.message + "\n" + e.stack)
            }
        })
    }

    function log(str) {
        if (process.debug) console.log(str);
    }

    program.start();

    process.on('uncaughtException', function(err) {
        console.log(err);
    });

}
