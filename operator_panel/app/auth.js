/**
 * Created by root on 15/04/14.
 */
var config = require("./../../config");
//var db     = require("./../../logic/database/database_handler");
db = {verifyApiKey: function(key, cb)
{
    if(key =="key") cb(null, true);
    else cb(null, false)
}};


module.exports.restrict = function restrict(req, res, next) {
    if (req.session.user) {
        next();
    } else {
        //req.session.error = 'Access denied!';
        res.redirect(config.NOT_LOGGEDIN_REDIR);
    }
};

module.exports.apiVerify = function apiVerify(req, res, next)
{
    //TODO database api verification and request validity check
    db.verifyApiKey(req.query.key, function(err, ok)
    {
        res.ok = ok;
        next();
    })
};