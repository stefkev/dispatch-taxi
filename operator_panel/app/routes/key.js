/**
 * Created by root on 13.07.14.
 */

var events = require("./../../../logic/client_lib/events");

module.exports.key = function(req, res)
{
    if(res.ok)
    {
        res.end(JSON.stringify(events));
    }
    else
    {
        res.end("throw new Error('Wrong API KEY');")
    }
};