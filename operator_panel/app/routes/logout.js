/**
 * Created by root on 15/04/14.
 */


module.exports.logout = function(request, response){
    request.session.destroy(function(){
        response.redirect('/');
    });
};