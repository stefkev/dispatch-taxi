/**
 * Created by root on 24/04/14.
 */
 
 process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
 
var events =
{
    makeRequest: 7,
    makeRadioRequest: 77,
    getZone:32,
    getData: "data",
    sendMessage: "msg",
    emergencyCancel: "cancel",
    remember: "remember",
    broadcast: "broadcast",
    prices: "prices"
};
var driver_sender = require("./../../logic/drivers/driver_sender");
var emergencyCancel = require("./../../logic/client_lib/events").requestEvents.emergencyCancel;
var clientHandler = require("./../../logic/clients/customer_handler");
var driver_list = require("./../../logic/drivers/drivers_list");
var chat = require("./../../logic/chat/chat_handler");
var db = require("./../../logic/database/database_handler");
var async = require("async");
var rideList = require("./../../logic/rides/ride_list");
var geolib = require("./../../node_modules/geolib");
var httpcall = require('http');
httpcall.createServer(function (req, res) { });

        function updateDriverPosition(id,pos,name){
        
                        //console.log("Zone update position:"+id +" Pos: "+ pos);
                        var options = {
                                host: '104.236.217.182',
                                port: 80,
                                path: '/dispatch/data/update_driver_position.php?driver='+id+'&position='+pos+'&zone='+name,
                                method: 'GET'
                            };

                            httpcall.request('http://104.236.217.182/dispatch/data/update_driver_position.php?driver='+id+'&position='+pos+'&zone='+name, function (res) {
                                res.setEncoding('utf8');
                                 res.on('data', function (chunk) {
                                    // console.log('BODY: ' + chunk);
                                 });
                            }).end();
        }

exports.handleData = function (sock, client, io) {
    console.log('welcome to data handling')
    // console.log(client)
    sock.reqId = null; 
    sock.on(events.makeRequest, function (data) {
        console.log(events.makeRequest)
        if(!( data.id && data.lat && data.lon && data.dest && data.vehicle && data.date &&
            data.minutesBefore &&  client && data.phonenumber)) {
            console.log(" === Error order ===");
            console.log(data);
            console.log(" === Error order ===");
            return;
        }
        console.log('will try to push data unique')
        sock.uniques.push(data.uniqueId);
         clientHandler.requestRide(
             data.id,
             data.lat,
             data.lon,
             data.fullname,
             data.dest,
             data.vehicle,
             data.date,
             data.minutesBefore,
             client,
             data.additional || "",
             data.phonenumber,
             data.uniqueId
         );
    });
    // sock.on(events.makeRadioRequest, function (data) {

    // sock.uniques.push(data.uniqueId);
    
            // clientHandler.requestRadioRide(
            // data.id,
            // data.lat,
            // data.lon,
            // data.fullname,
            // data.dest,
            // data.vehicle,
            // data.date,
            // data.minutesBefore,
            // client,
            // data.additional || "",
            // data.phonenumber,
            // data.uniqueId
        // );
    
    // this.from = [23.022505, 72.57136209999999];
    // this.to = ["23.2156354","72.63694149999992"];
    // this.stage = 1;
    // this.index = 0;

    // db.setRide(695, this);
     // db.reportEvent(4, 695, "1");  

        // });
        
        
    sock.on(events.getData, function (data) {
        console.log(events.getData)
        sock.client.operatorId = data.id;
        console.log(sock.client.operatorId)
        giveDataRefactored(sock);
    });
    sock.on(events.sendMessage, function (data) {
        console.log(events.sendMessage)
        console.log('tryong to send mesg')
        chat.sendMessage(0, data.id, data.msg);
    });
    sock.on(events.emergencyCancel, function (data) {
        console.log(events.emergencyCancel)
        var ride =  rideList.getList()[data.id];
        if(!ride || !ride.driver || !ride.customer){
            console.log("Cancelling ride tha should work but doesnt " + data)
            return;
        }
        db.reportEvent(emergencyCancel, ride.driver.id , [ride.from.join("|"), ride.to.join("|"), ride.customer.id, ride.customer.phonenumber].join("|"));
        rideList.emergencyCancelRide(data.id);
    });
    sock.on(events.remember, function(data){
        console.log(events.remember)
        if(!data.phone) return
        db.remember(data.phone, data)
    });
    //TODO FINISH ALL BELOW
    sock.on(events.getMessages, function (data) {
        console.log(events.getMessages)
        db.getMessages(data.from, data.to, function (err, result) {
            if (!err){
                sock.emit('messages', result);
            }
        })
    });

    sock.on(events.getPositionLog, function (data) {
        console.log(events.getPositionLog)
        db.getPositionLog(data.id, data.from, data.to, function (err, results) {
            sock.emit("positionLogs", results);
        })
    });
    sock.on(events.broadcast, function(data)
    {
        console.log(events.broadcast)
        chat.broadcast(-1, data)
    });
    sock.on(events.prices, function(data)
    {
        console.log(events.prices)
        db.setPrices(data);
    })
    sock.on(events.getZone, function(data)
    {
        console.log(events.getZone)
         var driverLists = [];
            var list = driver_list.list;
            console.log('test listng')
            console.log('==============')
            console.log(list)
            console.log('==============')
            var driver;
            for (var elementId in list) {
                driver = list[elementId];
                if(!driver.online || !driver.onDuty) continue;
                driverLists.push({
                    id: driver.id,
                    latitude: driver.latitude,
                    longitude: driver.longitude,
                    fullname: driver.fullname,
                    rpm: driver.rpm,
                    vehicle: driver.vehicle,
                    licensePlate: driver.licensePlate,
                    logintime: driver.logintime
                });
            }
        
        // Sort Driver
        
        driverLists.sort(function(a, b) {
            return parseFloat(a.logintime) - parseFloat(b.logintime);
        });
        
        // Get zone data 
        // geolib@2.0.20 node_modules/geolib
        var zone;
        //var driverLists = driver_list.getList();
        db.getZoneData(function(zoneData){
            zone = zoneData;
            
            for (var i = 0; i < driverLists.length; i++) {
            var isTrue = true;
            
            
                for (var j = 0; j < zone.length; j++) {
                
                    var objArray = [];
                   
                    var stringcords = zone[j].latitude;
                    var subcoords = stringcords.split(',');

                    var stringcords1 = zone[j].longitude;
                    var subcoords1 = stringcords1.split(',');

                    for (var jj = 0; jj < subcoords.length; jj++) {
                        var obj = {};
                        obj.lat = parseFloat(subcoords[jj]);
                        obj.lng = parseFloat(subcoords1[jj]);
                        objArray.push(obj)
                    }
                    zone[j].polydata = objArray;
                    
                    //console.log("Zone test:"+geolib.isPointInside({latitude: parseFloat(driverLists[i].latitude), longitude: parseFloat(driverLists[i].longitude)}, objArray));
                    if(geolib.isPointInside({latitude: parseFloat(driverLists[i].latitude), longitude: parseFloat(driverLists[i].longitude)}, objArray)){
                        //zone[j].polydata
                        //console.log("Zone test:"+driverLists[i]);
                        
                        driverLists[i].logintime = new Date(); 
                        
                        if(zone[j].Taxi == undefined){
                            zone[j].Taxi = [];
                        }
                        
                        //console.log("Zone test:"+zone[j].Zone_name);
                        zone[j].Taxi.push(driverLists[i]);
                        
                        if(driverLists[i].id != undefined){
                        //console.log("Zone update position:"+driverLists[i].id +" Pos: "+ zone[j].Taxi.length);
                            updateDriverPosition(driverLists[i].id ,zone[j].Taxi.length, zone[j].Zone_name);
                        }
                    }
                }   
            }
                
            
            console.log("Get Zone : Data :" + zone);
            debugger;
            sock.emit('getZone', { zone: zone });
        }); 
        
    });
        
};

function giveData(sock) {
    var allDrivers = [];
    var list = driver_list.list;
    var driver;
    for (var elementId in list) {

        driver = list[elementId];
        if(!driver.online || !driver.onDuty) continue;
        allDrivers.push({id: driver.id, lat: driver.latitude, lng: driver.longitude, fullname: driver.fullname, rpm: driver.rpm,logintime: driver.logintime});
    }

    db.getAllRemembered(function (err, remembered){
        db.getDriversCount(function (err, driversCount) {
            db.getFreeDriversCount(function (err, freeDriversCount) {
                db.getFinishedRidesCount(function (err, finishedRidesCount) {
                    db.getRides(function (err, rides) {
                        db.getMessages(-100,-1,function (err, messages) {
                            db.getHeat(function (err, heat) {
                                var arr = [];
                                for (var driverId in rides) {
                                    var driver = driver_list.getDriver(driverId);

                                    //if driver or ride no longer exists
                                    if (!driver || !driver.ride) continue;
                                    arr.push(
                                        {
                                            driverId: driverId,
                                            rideId: driver.ride.index,
                                            stage: JSON.parse(rides[driverId]).stage,
                                            lat: driver.latitude,
                                            lng: driver.longitude,
                                            driverName: driver.fullname,
                                            passengerName: driver.ride.customer.fullname,
                                            from: driver.ride.from,
                                            to: driver.ride.to
                                        });
                                }
                                sock.emit('data', {

                                    driverList: allDrivers,
                                    drivers: driversCount,
                                    free: freeDriversCount,
                                    finished: finishedRidesCount,
                                    rides: arr,
                                    heat: heat,
                                    messages: messages,
                                    remembered: remembered
                                });
                            });
                        })
                    })
                })
            })
        })
    })
}


var getAllDataFunctions = {
    remembered: db.getAllRemembered,
    driversCount: driver_list.getDriversCount,
    freeDriversCount: driver_list.getFreeDriversCount,
    finishedRidesCount: db.getFinishedRidesCount,
    rides: db.getRides,
    messages: function(cb){db.getMessages(-100,-1,cb)},
    heat: db.getHeat,
    prices: db.getPrices
};

function giveDataRefactored(sock) {

    var allDrivers = [];
    var list = driver_list.list;
    var driver;
    for (var elementId in list) {

        driver = list[elementId];
        if(!driver.online || !driver.onDuty) continue;
        allDrivers.push({
            id: driver.id,
            lat: driver.latitude,
            lng: driver.longitude,
            fullname: driver.fullname,
            rpm: driver.rpm,
            vehicle: driver.vehicle,
            licensePlate : driver.licensePlate,
            logintime: driver.logintime
        });
    }

    async.parallel(getAllDataFunctions, function(err, result){
        if(err) console.log("Err " + err);
        var arr = [];
        for (var driverId in result.rides) {
            var driver = driver_list.getDriver(driverId);

            //if driver or ride no longer exists
            if (!driver || !driver.ride) continue;
            arr.push(
                {
                    driverId: driverId,
                    rideId: driver.ride.index,
                    stage: JSON.parse(result.rides[driverId]).stage,
                    lat: driver.latitude,
                    lng: driver.longitude,
                    driverName: driver.fullname,
                    passengerName: driver.ride.fullname,
                    from: driver.ride.from,
                    to: driver.ride.to,
                    vehicle: driver.vehicle,
                    passengerPhone: driver.ride.phonenumber,
                    licensePlate : driver.licensePlate,
                    logintime: driver.logintime,
                    additionalInfo: driver.ride.additionalInfo,
                    dispatchtime: driver.ride.dispatchtime
                });
        }
        sock.emit('data', {

            driverList: allDrivers,
            drivers: result.driversCount,
            free: result.freeDriversCount,
            finished: result.finishedRidesCount,
            rides: arr,
            heat: result.heat,
            messages: result.messages,
            remembered: result.remembered,
            prices : result.prices
        });
    })
}