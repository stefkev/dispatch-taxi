var map;

function mapsPageLoad(){
    var mapOptions = {
        zoom: 4,
        center: { lat: 0 , lng:0}
    };
    map = new google.maps.Map(document.getElementById('location'), mapOptions);
    initSocket();
    $("#basic").width("100%").height("100%").gmap3({
        map:{
            address:"Minsk, Belarus",
            options:{
                zoom: 4,
                mapTypeId: google.maps.MapTypeId.TERRAIN
            }
        }
    });
    $('#location').width("100%").height("100%");
    $('#address').keypress(function(e) {
        if(e.which == 13) {
            var addr = $(this).val();
            if ( !addr || !addr.length ) return;
            $("#location").gmap3({
                getlatlng:{
                    address:  addr,
                    callback: function(results){
                        if ( !results ) return;
                        $(this).gmap3({
                            marker:{
                                latLng:results[0].geometry.location,
                                dragable: true
                            },
                            map:{
                                options:{
                                    center: results[0].geometry.location,
                                    zoom: 18
                                }
                            }
                        });
                    }
                }
            });
        }
    });

    $('#vector-world').width("100%").height("100%").vectorMap({
        map: 'world_en',
        backgroundColor: '#a5bfdd',
        borderColor: '#818181',
        borderOpacity: 0.25,
        borderWidth: 1,
        color: '#f4f3f0',
        enableZoom: true,
        hoverColor: $orange,
        hoverOpacity: null,
        normalizeFunction: 'linear',
        scaleColors: ['#b6d6ff', '#005ace'],
        selectedColor: $red,
        selectedRegion: null,
        showTooltip: true,
        onRegionClick: function(element, code, region){
            var $modal = $("#myModal");
            $modal.find(".modal-body p").html('You clicked <strong>'
                + region
                + '</strong> which has the code: '
                + code.toUpperCase());
            $modal.modal('show');
        }
    });
    function vectorDetailed(map){
        //jqvmap has a problem with destroying itself
        //so clear it hard way
        $('#vector-detailed').replaceWith("<div id='vector-detailed'></div>");
        var $map = $('#vector-detailed');
        $map.width("100%").height("100%").vectorMap({
            map: map,
            enableZoom: true,
            hoverColor: $orange,
            hoverOpacity: null,
            normalizeFunction: 'linear',
            scaleColors: ['#b6d6ff', '#005ace'],
            selectedColor: $red,
            selectedRegion: null,
            showTooltip: true,
            onRegionClick: function(element, code, region){
                var $modal = $("#myModal");
                $modal.find(".modal-body p").html('You clicked "'
                    + region
                    + '" which has the code: '
                    + code.toUpperCase());
                $modal.modal('show');
            }
        });
        if (map == 'europe_en'){
            //$map.data('mapObject').zoomIn();
        }
    }

    vectorDetailed('europe_en');


    $(".selectpicker").selectpicker().on("change", function(){
        vectorDetailed($(this).val());
    });
    //selectpicker doesn't seem to be flexible enough (can't change template), so need to replace span.caret externally
    $('.selectpicker + .bootstrap-select span.caret').replaceWith("<i class='fa fa-caret-down'></i>");
    $('.selectpicker + .bootstrap-select span.pull-left').removeClass("pull-left");
}

$(function(){
    function loadScript() {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = 'http://maps.google.com/maps/api/js?sensor=false&language=en&' +
            'callback=mapsPageLoad';
        document.body.appendChild(script);
    }

    loadScript();

    PjaxApp.onPageLoad(mapsPageLoad);
});
var socket;
var taxis = {};
function initSocket()
{
    initButtons();
    var local = "http://localhost:8080";
    var remote = 'http://50.74.111.102:8080';
    socket = io.connect(local);
    socket.on('taxi',function(msg)
    {
        addMarker(msg.split(" "));
    });
    socket.on('move',function(msg)
    {
        handleMoveData(msg);
        console.log(msg);
    });
    socket.on('stage', function(msg)
    {
        console.log("STAGE", JSON.stringify(msg))
    });
    socket.on('remove',function(id)
    {
        console.log("removed");
        taxis[id].setMap(null);
        delete taxis[id];
    });
}
function sendRequest()
{
    socket.write()
}
function handleMoveData(data)
{
    console.log(taxis);
    var cake = data.split(" ");
    var index = cake[0];
    var lat = cake[1];
    var lon = cake[2];
    if(taxis[index])
    {
        taxis[index].position = new google.maps.LatLng(lat,lon);
    }
    else
    {
        addMarker(lat, lon, index)
    }
}
function addMarker(lat, lon, index)
{
    var myLatlng = new google.maps.LatLng(lat,lon);

    var mark = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: 'Hello World!'
    });
    mark.setIcon('http://maps.google.com/mapfiles/ms/icons/yellow-dot.png');
    taxis[index] = mark;

    return mark
}
function addMarkerAddress(address)
{
    $('#location').gmap3({
        marker:{
            address: address
        }
    });
}
function initButtons()
{
    $("#dispatchBt").on('click',function()
    {
        var cb = function(pos, dest) {
            var lat1 = pos.lat();
            var lon1 = pos.lng();

            var min = 1000000000;

            var closestId;
            var dist;
            var lat2;
            var lon2;
            for (var taxiId in taxis) {
                var taxi = taxis[taxiId];
                lat2 = taxi.getPosition().lat();
                lon2 = taxi.getPosition().lng();

                console.log("AAAA", lat2, lon2);
                dist = haversine(lat1, lon1, lat2, lon2);
                if (dist < min) {
                    min = dist;
                    closestId = taxiId
                }
            }
            if(closestId == undefined)
            {
                console.log("NO RIDER");
                return
            }
            socket.emit(7,
                {
                    id: closestId,
                    lat: lat1,
                    lon: lon1,
                    dest: dest.lat().toString() + "," + dest.lng().toString()
                });
        };
        geoCode($("#loc").val(), function(err, loc1)
        {
            geoCode($("#dest").val(), function(err2, loc2)
            {
                if(err) throw err;
                if(err2) throw err2;
                cb(loc1, loc2)
            })
        })
    });
}
function haversine(lat1, lon1, lat2,lon2)
{
    var R = 6371; // km
    var dLat = toRad((lat2-lat1));
    var dLon = toRad((lon2-lon1));
    lat1 = toRad(lat1);
    lat2 = toRad(lat2);

    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    return Math.round(R * c*1000)/1000;
}
function toRad(num)
{
    return num * (Math.PI/180)
}
function geoCode(address, cb)
{
    $("#location").gmap3({
        getlatlng: {
            address: address,
            callback: function (results) {
                console.log(results);
                if (!results) cb(new Error("no result", null));
                else cb(null ,results[0].geometry.location)
            }
        }
    });
}