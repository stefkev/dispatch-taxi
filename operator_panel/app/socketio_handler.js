/**
 * Created by root on 18/05/14.
 */
var config = require("./../../config");
var Client          = require('./../../client_handler/client');
var incoming_data_handler    = require("./incoming_data_handler");
var memored = require('memored')


var sioport = config.SOCKET_IO_PANEL_PORT;
var io = require('socket.io')(sioport,{log:false});
var redis = require('socket.io-redis');
io.adapter(redis({ host: 'localhost', port: 6379 }));
console.log('testatata')
console.log("OPERATOR SOCKET.IO SERVER LISTENING ON PORT " + sioport);

// var ioClientList = [];

memored.store('ioClientList', [])
debugger;
var sockets = io;
sockets.on('connection',function(sock)
{
    var address = sock.handshake.address;
    // console.log('adress');
    // console.log(address)
    var client = new Client.SioClient(address.address,address.port,sock, true);
    // console.log('client');
    // console.log(client)
    sock.client = client;
    console.log('sock client');
    console.log(sock.client);
    console.log('=a=a=a=a=a=a==a=a=a')
    sock.uniques = [];
    console.log('will read io client list')
    memored.read('ioClientList', function(err, ioClientList){
        ioClientList.push(client);
        console.log(err);
        memored.store('ioClientList', ioClientList)
    })
    console.log('inc data hadnler');

    incoming_data_handler.handleData(sock, client, io);
    console.log('===========================================================================')
    sock.on('disconnect',function()
    {
        memored.read('ioClientList', function(err, ioClientList){
            console.log('-------------------')
            console.log(ioClientList)
            console.log('-------------------')
            delete ioClientList[ioClientList.indexOf(client)];
            memored.store('ioClientList', ioClientList)
        })
    });
});

//OBSERVER
var driverlist = require("./../../logic/drivers/drivers_list");
driverlist.subscribe(function (data)
{
    console.log('data');
    console.log(data);
    console.log('----------------------')
    try {
        console.log('data.event');
        console.log(data.event)
        switch (data.event) {
            case driverlist.events.add:
                var driver = data.driver;
                var obj = {
                    id: driver.id,
                    lat: driver.latitude,
                    lng: driver.longitude,
                    fullname: driver.fullname,
                    licensePlate : driver.licensePlate
                };

                sendAll('taxi', obj);

                break;
            case driverlist.events.move:
                if (!data.driver)return;

                sendAll('move', data.driver.id.toString() +
                    " " + data.driver.latitude.toString() +
                    " " + data.driver.longitude.toString());
                break;
            case driverlist.events.remove:
                if (!data.driver) return;
                sendAll('remove', data.driver.id.toString());
                break;


            case driverlist.events.disconnect:
                sendAll('disconnect', data.id);
                break;
            case driverlist.events.message:
                sendAll("msg", {id: data.driver.id, msg: data.msg});
                break;
            case driverlist.events.success:
                io.eio.clients.forEach(function (socket) {
                    if(socket.uniques.indexOf(data.uniqueId) != -1) {
                        //remove the id
                        socket.uniques.splice(socket.uniques.indexOf(data.uniqueId), 1);
                        console.log("Sending success"+ data.uniqueId);
                        socket.emit("success", data.uniqueId);
                    }
                });
                break;
            case driverlist.events.reject:
                //console.log(data);

                // if(process.debug) {
                    // console.log("Pending");
                    // console.log(data);
                // }
                io.eio.clients.forEach(function (socket) {
                    //console.log("REQ ID : " + socket.reqId);
                    if(socket.uniques.indexOf(data.uniqueId) != -1) {
                        socket.uniques.splice(socket.uniques.indexOf(data.uniqueId), 1);
                        console.log("Sending reject"+ data.uniqueId);
                        socket.emit("reject", data.uniqueId);
                    }
                });
                break;
            case driverlist.events.sms:
                io.emit("sms", data.id);
                break;

            default :
        }
    }
    catch (e)
    {
        console.warn("Error in observer " + e);
        console.warn(e.stack)
    }
});
function sendAll(event,data)
{
    io.emit(event,data);
}

var ridelist = require("./../../logic/rides/ride_list");
ridelist.subscribe(function (data)
{
    switch(data.event)
    {
        case ridelist.events.add:
/*            sendAll('add',
                {
                    driverId: data.ride.driver.id,
                    lat: ride.customer.latitude,
                    lng: ride.customer.longitude,
                    rideId: data.ride.index
                });*/
            break;
        case ridelist.events.remove:
            break;
        case ridelist.events.updateState:
            sendAll("stage",
                {driverId: data.ride.driver.id,
                    from: data.ride.from,
                    to: data.ride.to,
                    rideId : data.ride.index,
                    stage:data.ride.stage,
                    driverName : data.ride.driver.fullname,
                    passengerName : data.ride.fullname,
                    passengerPhone: data.ride.phonenumber,
                    additionalInfo: data.ride.additionalInfo,
                    dispatchtime: data.ride.dispatchtime
                } );
            break;

    }
});