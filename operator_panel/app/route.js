/**
 * Created by root on 15/04/14.
 */

var index = require("./routes/index"),
    login = require("./routes/login"),
    logout= require("./routes/logout"),
    panel = require("./routes/panel"),
    stats = require("./routes/stats"),
    maps  = require("./routes/maps"),
    key   = require("./routes/key"),
    auth  = require("./auth");

exports = module.exports = function(app, middleware)
{
    app.get("",index.index);
    app.get('/', index.index);
    app.get('/index', index.index);
    app.get('/index.html', panel.panel);
    app.get('/login', login.login);
    app.post('/login', login.login_auth);
    app.get('/logout', logout.logout);
    app.get('/panel', middleware, panel.panel);
    app.get('/stat_charts.html', stats.charts);
    app.get('/component_maps.html', maps.map);
    app.get('/apikey', auth.apiVerify, key.key);
    app.get('*',function(req,res)
    {
        res.render("special_404.ejs");
    })

};

