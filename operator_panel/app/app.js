var express         = require('express');
var connect         = require('connect');
var config          = require('./../../config');

var app = express();


var PORT = config.HTTP_PORT;

app.use(express.static(__dirname + '/public'));
app.use(connect.middleware.bodyParser());
app.use(connect.middleware.cookieParser('shhhh, very secret'));
app.use(connect.middleware.session());
app.engine('html', require('ejs').renderFile);
app.set("views", __dirname+ '/views');
app.set("view engine",'ejs');

var router = require("./route.js");
var auth = require("./auth.js");

router(app, auth.restrict);

app.listen(PORT, function(){
    console.log('HTTP Server running on '+ PORT);
});

