/**
 * Created by Iraasta on 04.01.14.
 */
var main = {};
module.exports = main;

//-----------------------------
var stringify   = require("json-stringify-safe");
var path = require("path");
var config = require("./../config");
//

/**
 *
 * @param obj
 * @param {string} name
 */
main.add = function (obj, name) {
    vars[name] = (obj);
};

var m_port = config.ADMIN_PANEL_PORT;
var http = require('http');
var fs = require('fs');
var index = fs.readFileSync(path.join(__dirname, 'index.html'));
var vars = {};
main.vars = vars;

http.createServer(function (req, res) {

    res.writeHead(200, {'Content-Type': 'text/html'});

    startWrite(res);
    writeVar(res,"response",vars);
    endWrite(res);

    res.end(index);
}).listen(m_port);


function startWrite(res)
{
    res.write("<script>");
}
function writeVar(res,name,val)
{
    res.write("var "+name+ " = "+ stringify(val,null,2) +";");
}
function endWrite(res)
{
    res.write("</script>");
}