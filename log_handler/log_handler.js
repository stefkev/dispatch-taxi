/**
 * Created by Iraasta on 07.01.14.
 */
var log_handler = {};
module.exports = log_handler;

//========IMPORTS============
var databaseHandler         = require("./../logic/database/database_handler");
//==========END==============
/**
 *
 * @param content
 * @param tags
 * @constructor
 */
log_handler.Log = function(content, tags)
{
    this.date = new Date();
    this.tags = tags;
    try{
        this.content = JSON.stringify(content);
        this.tagString = tags.join(" ");
    }
    catch(err){

    }
};
/**
 *
 * @param {string} content
 * @param {Array} tags
 * @param {function} cb
 */
log_handler.log = function(content,tags,cb)
{
    var log = new log_handler.Log(content,tags);
    try{
        databaseHandler.insert('logs',{date:log.date.toJSON(),content:log.content,tags:log.tagString},function(err,result)
        {
            if(err)cb(err);
        });
    }
    catch(err)
    {
        cb(new Error("Error logging to db"));
    }
    return log;
};