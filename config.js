/**
 * Created by root on 20/04/14.
 */

config = {};
module.exports = config;

// GENERAL OPTIONS
config.AUTO_CANCEL_DELAY    = 30000;
config.RATE_MAX             = 10;
config.TCP_PORT             = 25001;
config.ADMIN_PANEL_PORT     = 1000;
config.FRAME_LENGTH_LENGTH  = 1;
config.USE_ERROR_LOGGING    = true;
config.DRIVER_LIST_REFRESH  = 500;

config.POSITION_LOG_ENABLED = true;
config.POSITION_LOG_TIMOUT  = 60000;
config.USES_SOCKETIO        = true;
config.SOCKET_IO_PORT       = 25002;
config.CHARGE_DAY           = 4;
config.STACK_MAX_RECURSION  = 10;


//DB
config.DB_MODULE = "redis";
config.DB_AUTH_MODULE = "mySQL";

    //MYSQL
    config.SQL_HOST                 = '127.0.0.1';
    config.SQL_USER                 = 'root';
    config.SQL_PASSWORD             = 'widetech@789';
    config.SQL_DATABASE             = 'AutoDispatch';
    config.SQL_CLIENTS_TABLE        = 'customers';
    config.SQL_DRIVERS_TABLE        = 'drivers';
    config.SQL_POSTION_LOG          = 'DevicePositions';
    config.SQL_EVENT_LOG            = "eventlog";
    config.SQL_WORKORDER_LOGINS     = "drivers"; //"workorder_logins";
    config.SQL_WORKORDER_POSITIONS  = "workorder_positions";

    //REDIS
    config.RED_CLIENTS_SET          = 'h_clients';
    config.RED_DRIVERS_SET          = 'h_drivers';
    config.RED_LOG_LIST             = 'z_logs';
    config.RED_DRIVERS_COUNT        = 'drivers';
    config.RED_FREE_DRIVERS_COUNT   = 'free_drivers';
    config.RED_FINISHED_RIDES       = 'rides';
    config.RED_RIDE_LIST            = 'h_rides';
    config.RED_HEATMAP              = 'h_heatmap';
    config.RED_MESSAGES_LIST        = 'l_messages';
    config.RED_LAST_CHARGE_DATE     = "last_charge";
    config.RED_RESERVATIONS_HASHMAP = "h_reservations";
    config.RED_WORKORDER_ROUTE      = "h_workorder_route";
    config.RED_WORKORDER_ACQUIRED   = "h_workorder_acquired";
    config.RED_REMEMBERED           = "h_remembered";
    config.RED_PRICES               = "prices";
    config.Zone_List                = "zones"

    //elastic DB caching
    //
    config.ELASTIC_HOST             = '';
    config.ELASTIC_USER             = '';
    config.ELASTIC_PASSWORD         = '';
    
//Driver List
config.ALL_CABS             = false;
config.CAB_COUNT            = 10;
config.MAX_SQUARE_DISTANCE  = 10000000000;
config.MULTIPLIER           = 10000;

config.RIDE_STAGES          = {
    pending : 0,
    waitingForPickup: 1,
    driving: 2,
    finished: 3
};

config.WORK_ORDER = {
    SOCKET_IO_PORT: 9001
};
// RECONNECTION HANDLER 
//config.RECONNECT_DELAY      = 800000; //miliseconds
config.RECONNECT_DELAY      = 100;


//OPERATOR PANEL AND EXPRESS
config.SOCKET_IO_PANEL_PORT       = 9008;
config.HTTP_PORT            = 81;
config.NOT_LOGGEDIN_REDIR   = "/login";

config.HEATMAP_WEIGHTS =
{
    ONE_JUMP : 1,
    FAIL : 10
};
config.HEATMAP_ENABLED      = true;
config.HEATMAP_CLEAN_TIME = 900000;
config.HEATMAP_REDUCTION_PERCENT = 50;

//==================== ARGUMENTS =================================
for(var id in process.argv){
    if(process.argv.hasOwnProperty(id)) {
        var cmd = process.argv[id];
        if (/--tcp=/.test(cmd)) {
            config.TCP_PORT = parseInt(cmd.split(/--tcp=/)[1]);
        }
        if (/--sio=/.test(cmd)) {
            config.SOCKET_IO_PANEL_PORT = parseInt(cmd.split(/--sio=/)[1]);
        }
    }
}

config.MAILER_LOGIN     = "lightdispatch@gmail.com";
config.MAILER_PASSOWRD  = "xjyquiaholiqcgyw";

config.SMS_PROVIDER_LIST = [
    "txt.att.net" ,
    "mymetropcs.com",
    "tmomail.net",
    "messaging.sprintpcs.com",
    "vtext.com"
];
config.SMS_CONTENT = "Your cab is outside. Thank You for Your using our service. Light Dispatch";

config.VEHICLE_WEIGTH = {
    "sedan" : 0,
    "suv" : 1,
    "suv2" : 2
}