/**
 * Created by Iraasta on 18.12.13.
 */
var clientList = require("./client_list");
var config = require("./../config");
var customer =require("./../logic/clients/customer");
/**
 *
 * @param {string} ip
 * @param {int} port
 * @param sock
 * @constructor
 */
var Client = function(ip,port,sock)
{
    this.index = ip+":"+port;
    clientList.addClient(this);
    this.sock = sock;
};
Client.prototype.send = function(data)
{
    console.warn("send is obsolete use sendEvent instead");
    if(!this.checkIfOnline())return;
    this.sock.write(data);
};
/**
 *
 * @param event
 * @param {String|Array} [data]
 *
 */
Client.prototype.sendEvent = function(event,data)
{
    if(Array.isArray(data)) data = data.join("|");
    if(!this.checkIfOnline()){
        console.log("Not online");
        return;
    }
    var b;
    if(!data)
    {
        b = new Buffer(1);
        b.writeInt8(1,0);
        this.sock.write(b);
        this.sock.write(String.fromCharCode(event));
    }
    else
    {
        b = new Buffer(4);
        b.writeUInt32BE(data.length+1,0);
        this.sock.write(b.slice(4-config.FRAME_LENGTH_LENGTH));
        this.sock.write(String.fromCharCode(event)+ data);
    }
    if(process.debug) console.log("SEND Event: " + event + " MESSAGE: " + data)
};
Client.prototype.checkIfOnline = function()
{
    if (!this.sock.writable){
        this.disconnect();
        return false;
    }
    return true;
};
Client.prototype.disconnect = function()
{
    if(process.debug)console.log((new Date).toGMTString() + " disconnected " + this.index);
    this.sock.destroy();
    clientList.removeClient(this.index)
};

//-------------------------
//--Socket.IO CLIENT
//------------------------

var SioClient = function(ip,port,sock, isOperator)
{
    this.isSIO = true;
    this.isOperator = isOperator;
    this.index = ip+":"+port;
    if(!isOperator)clientList.addClient(this);
    this.sock = sock;
    this.disconected = false;
    if(isOperator)this.customer = new customer.Customer(this, 0, "Manual dispatch", 0);
};
SioClient.prototype.sendEvent = function(event,data)
{
    if(process.debug)
    {
        if(Buffer.isBuffer(data))console.log("EVENT: " + event + " DATA: " + data.toJSON() + " SENT TO CLIENT");
        else                     console.log("EVENT: " + event + " DATA: " + data          + " SENT TO CLIENT");
    }
    this.sock.emit(event,data);
};
SioClient.prototype.checkIfOnline = function()
{
    return !this.disconected;
};
SioClient.prototype.disconnect = function()
{
    if(!this.isOperator)
    {
        clientList.removeClient(this.index)
    }
};

module.exports.Client = Client;
module.exports.SioClient = SioClient;
