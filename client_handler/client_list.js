/**
 * Created by Iraasta on 18.12.13.
 */
//var ClientBaseClass = require("./client").Client;
var memored = require("memored")
var clientList = {};
memored.store('client', [])
module.exports = clientList;

// var list = {};
clientList.list = memored.read('client', function(err, list) {
    return list
});

clientList.getList = function() {
    memored.read('client', function(err, list) {
        return list
    })
};
clientList.addClient = function(client) {
    memored.read('client', function(err, list) {
        list[client.index] = client;
        memored.store('client', list);
    })

};
clientList.getClient = function(client) {
    return memored.read('client', function(err, list) {
        return list[client.remoteAddress + ":" + client.remotePort];
    })

};
/**
 *
 * @param client
 * @returns {boolean}
 */
clientList.checkIfLoggedIn = function(client) {
    return memored.read('client', function(err, list) {
        return !!list[client.index];
    })

};
/**
 *
 * @param {String} index
 */
clientList.removeClient = function(index) {
    memored.read('client', function(err, list) {
        delete list[index];
        memored.store('client', list)
    })

};
clientList.changeClientClass = function() {
    //
};
